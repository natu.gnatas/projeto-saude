<?php
session_start();

if(isset($_POST['sair_btn']))
{
    //session_destroy();
    unset($_SESSION['auth']);
    unset($_SESSION['auth_role']);
    unset($_SESSION['auth_user']);

    $_SESSION['menssagem'] ="Desconectado com sucesso!";
    header("Location: index.php");
    exit(0);
}
?>