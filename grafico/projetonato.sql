-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 08-Fev-2022 às 16:04
-- Versão do servidor: 10.4.22-MariaDB
-- versão do PHP: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `projetonato`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tblsensores`
--

CREATE TABLE `tblsensores` (
  `id` int(11) NOT NULL,
  `humedad` float(4,2) NOT NULL,
  `temperatura` float(4,2) NOT NULL,
  `evento` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tblsensores`
--

INSERT INTO `tblsensores` (`id`, `humedad`, `temperatura`, `evento`) VALUES
(1, 41.50, 30.20, '2022-01-18 10:59:36'),
(2, 51.50, 60.20, '2022-01-10 12:59:36'),
(3, 42.00, 28.70, '2022-01-20 14:59:36'),
(4, 20.00, 31.00, '2022-01-21 16:59:36'),
(5, 61.50, 30.20, '2022-01-22 17:59:36'),
(6, 72.00, 28.70, '2022-01-23 18:59:36'),
(7, 50.00, 11.00, '2022-01-24 19:59:36'),
(8, 92.00, 28.70, '2022-02-08 08:59:36'),
(9, 50.00, 91.00, '2022-02-09 09:59:36'),
(10, 15.00, 26.70, '2022-02-18 10:59:36');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `tblsensores`
--
ALTER TABLE `tblsensores`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `tblsensores`
--
ALTER TABLE `tblsensores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
