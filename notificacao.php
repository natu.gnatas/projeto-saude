<!-- Alerta dos valores criticos-->
<li class="nav-item dropdown no-arrow mx-1">
    <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-bell fa-fw"></i>
        <!-- Counter - Alerts -->
        <span class="badge badge-danger badge-counter"><?php echo mysqli_num_rows(mysqli_query($conn,"SELECT * FROM alerta WHERE view=0")) ?></span>
    </a>
    <!-- Dropdown - Alerts -->
    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
        <h6 class="dropdown-header">
        Alertas do Sistema
        </h6>

        <?php

        $res=mysqli_query($conn,"SELECT * FROM alerta ORDER BY data DESC LIMIT 5");
        if($res){
            while ($linha=mysqli_fetch_assoc($res)) {
            echo '<a class="dropdown-item d-flex align-items-center" href="#">
                <div class="mr-3">
                    <div class="icon-circle bg-warning">
                    <i class="fas fa-exclamation-triangle text-white"></i>
                    </div>
                </div>
                <div>
                    <div class="small text-gray-500">'.$linha['data'].'</div>
                    '.$linha['menssagem'].'
                </div>
                </a>';
            }
        }

        ?>
        <a class="dropdown-item text-center small text-gray-500" href="alerta.php">Ver todas alertas</a>
    </div>
</li>