<?php
require_once("conexion.php");
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <script type="text/javascript">
            setTimeout('document.location.reload()',5000)//tempo que a página recarega
        </script>
        <title>Highcharts Example</title>

        <script type="text/javascript" src="resources/jquery.js"></script>
        <style type="text/css">
    
        </style>
        <script type="text/javascript">
            $(function () {
                $('#container').highcharts({
                    title: {
                        text: 'Temperatura do paciente',
                        x: -20 //center
                    },
                    subtitle: {
                        // text: 'SENSORIAMENTO REMOTO EM TEMPO REAL com Microcontrolador esp32',
                        x: -20
                    },
                    xAxis: {
                        categories: [
                        <?php
                            $sql = " select reading_time from sensordata order by id desc limit 128 ";
                            $result = mysqli_query($connection, $sql);
                            while($registros = mysqli_fetch_array($result)){
                                ?>
                                    '<?php echo  $registros["reading_time"]?>',
                                <?php
                            }
                        ?>
                        ]
                    },
                    yAxis: {
                        title: {
                            text: 'Temperatura [°C]'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        valueSuffix: ' °C'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [
                    {   name: 'Temperatura',
                        data: [
                        <?php
                            $query = " select value1 from sensordata order by id desc limit 128 ";
                            $resultados = mysqli_query($connection, $query);
                            while($rows = mysqli_fetch_array($resultados)){
                                ?>
                                    <?php echo $rows["value1"]?>,
                                <?php
                            }
                        ?>]
                    }
                    ]
                });
            });
        </script>
    </head>
    <body>
<script src="resources/highcharts.js"></script>
<script src="resources/exporting.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

    </body>
</html>