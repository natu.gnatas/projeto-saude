<?php
require_once("conexion.php");
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <script type="text/javascript">
            setTimeout('document.location.reload()',5000)//especificamos los milisegundos en que la p�gina se recarga
        </script>
        <title>Highcharts Example</title>

        <script type="text/javascript" src="resources/jquery.js"></script>
        <style type="text/css">
    
        </style>
        <script type="text/javascript">
            $(function () {
                $('#container').highcharts({
                    title: {
                        text: 'Batimento cardíaco do paciente',
                        x: -20 //center
                    },
                    subtitle: {
                        // text: 'SENSORIAMENTO REMOTO EM TEMPO REAL com Microcontroladores',
                        x: -20
                    },
                    xAxis: {
                        categories: [
                        <?php
                            $sql = " select created_at from anotacao_paciente order by id desc limit 128 ";
                            $result = mysqli_query($connection, $sql);
                            while($registros = mysqli_fetch_array($result)){
                                ?>
                                    '<?php echo  $registros["created_at"]?>',
                                <?php
                            }
                        ?>
                        ]
                    },
                    yAxis: {
                        title: {
                            text: 'Batimento cardíaco [bpm]'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#fa0afe'
                        }]
                    },
                    tooltip: {
                        valueSuffix: ' bpm'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [
                    {   name: 'Batimento Cardíaco',
                        data: [
                        <?php
                            $query = " select value2 from sensordata order by id desc limit 128 ";
                            $resultados = mysqli_query($connection, $query);
                            while($rows = mysqli_fetch_array($resultados)){
                                ?>
                                    <?php echo $rows["value2"]?>,
                                <?php
                            }
                        ?>]
                    }
                    ]
                });
            });
        </script>
    </head>
    <body>
<script src="resources/highcharts.js"></script>
<script src="resources/exporting.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

    </body>
</html>