<?php
require_once("conexion.php");
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <!-- <script type="text/javascript">
            setTimeout('document.location.reload()',30000)//especificamos los milisegundos en que la p�gina se recarga
        </script> -->
        <title>Highcharts Example</title>

        <script type="text/javascript" src="resources/jquery.js"></script>
        <style type="text/css">
        
    
        </style>
        <script type="text/javascript">
            setTimeout('document.location.reload()',4000)//especificamos los milisegundos en que la p�gina se recarga
            $(function () {
                $('#container').highcharts({
                    title: {
                        text: 'Grafico de Monitorização em Tempo Real',
                        x: -20 //center
                    },
                    subtitle: {
                        // text: 'Monitorização remoto em tempo com Microcontroladores',
                        x: -20
                    },
                    xAxis: {
                        categories: [
                        <?php
                            $sql = " select reading_time from sensordata order by id desc limit 128 ";
                            $result = mysqli_query($connection, $sql);
                            while($registros = mysqli_fetch_array($result)){
                                ?>
                                    '<?php echo  $registros["reading_time"]?>',
                                <?php
                            }
                        ?>
                        ]
                    },
                    yAxis: {
                        title: {
                            text: 'Amplitudes '
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#000'
                        }]
                    },
                    tooltip: {
                        valueSuffix: ' esp32'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [
                        // Grafico de Temperatura
                    {
                        tooltip: {
                            valueSuffix: ' °C'
                        },
                        name: 'Temperatura',
                        data: [
                        <?php
                            $query = " select value3 from sensordata order by id desc limit 128 ";
                            $resultados = mysqli_query($connection, $query);
                            while($rows = mysqli_fetch_array($resultados)){
                                ?>
                                    <?php echo $rows["value3"]?>,
                                <?php
                            }
                        ?>]
                    },
                      // Grafico de Batimento cardíaco
                    {   
                        tooltip: {
                            valueSuffix: ' bpm'
                        },
                        name: 'B. cardíaco',
                        data: [
                        <?php
                            $query = " select value4 from sensordata order by id desc limit 128 ";
                            $resultados = mysqli_query($connection, $query);
                            while($rows = mysqli_fetch_array($resultados)){
                                ?>
                                    <?php echo $rows["value4"]?>,
                                <?php
                            }
                        ?>]
                    },
                    
                    ]
                });
            });
        </script>
    </head>
    <body>
<script src="resources/highcharts.js"></script>
<script src="resources/exporting.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

    </body>
</html>