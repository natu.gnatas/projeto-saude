<?php
session_start();

if(isset($_SESSION['auth']))
{
    $_SESSION['menssagem'] = "Você já está conectado";
    header("Location: index.php");
    exit(0);
}

include('includes/header.php');
//include('includes/navbar.php');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SGSaude</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" />
    <link rel="stylesheet" href="css/styleform.css" />
  </head>
  <body class="bg-info">
    <div class="container">
      <!-- formulário pa Login -->

      <div class="row justify-content-center wrapper" id="login-box">
        <div class="col-lg-10 my-auto sombreados">
          <div class="row">

          <?php include('menssagem.php'); ?>

            <div class="col-lg-7 bg-white p-4">
              <h1 style="color: #f923ee" class="text-center font-weight-bold">Autenticação</h1>
              <hr class="my-3" />
              <form class="form" action="code_login2.php" method="post">
                <div class="input-group input-group-lg form-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text rounded-0"><i class="far fa-envelope fa-lg fa-fw"></i></span>
                  </div>
                  <input type="email" id="email" name="email" class="form-control rounded-0" placeholder="E-Mail" required />
                </div>
                <div  class="input-group input-group-lg form-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text rounded-0"><i class="fas fa-key fa-lg fa-fw"></i></span>
                  </div>
                  <input type="password" id="senha" name="senha" class="form-control rounded-0" minlength="5" placeholder="Senha" required autocomplete="off" />
                </div>
                <div class="form-group clearfix">
                  <div class="custom-control custom-checkbox float-left">
                    <input type="checkbox" class="custom-control-input" id="customCheck" name="rem" />
                    <label style="color: #f923ee" class="custom-control-label" for="customCheck">Lembrar-me</label>
                  </div>
                  <div class="forgot float-right">
                    <a style="color: #f923ee" href="#" id="forgot-link">Esqueceu sua Senha?</a>
                  </div>
                </div>
                <div class="form-group-mb-3">
                  <input type="submit" name="login2_btn" id="login2_btn" value="Entrar" class="btn btn-primary btn-lg btn-block botao" />
                </div>
              </form>
            </div>
            <div class="col-lg-5 d-flex flex-column justify-content-center cores p-4">
               
              <h1 class="text-center font-weight-bold text-white">Cuide do seu Paciente!</h1>
              <!--<i class="icofont-heart-beat-alt"></i>-->
              
              <hr class="my-3 bg-light myHr" />
              <p class="text-center font-weight-bolder text-light lead">Insira os seus dados pessoais para ter acesso ao sistema!</p>
              <!-- <button class="btn btn-outline-light btn-lg align-self-center font-weight-bolder mt-4 myLinkBtn" id="register-link">Registrar-me</button> -->
             <!--<i class="fa-solid fa-heart-pulse"></i>-->
            </div>
          </div>
        </div>
      </div>
      <!-- Fim di form Login -->

      <?php
       include('includes/footer.php');
       ?>

      <!-- Caso bu kre regista utlizador di li mesmo -->
      <div class="row justify-content-center wrapper" name="registrar_btn" id="register-box" style="display: none;">
        <div class="col-lg-10 my-auto sombreados">
          <div class="row">
            <div class="col-lg-5 d-flex flex-column justify-content-center cores p-4">
              <h1 class="text-center font-weight-bold text-white">Bem vindo!</h1>
              <hr class="my-4 bg-light myHr" />
              <p class="text-center font-weight-bolder text-light lead">Faça Login para o acesso ao nosso sistema.</p>
              <button class="btn btn-outline-light btn-lg font-weight-bolder mt-4 align-self-center myLinkBtn" id="login-link">Registar</button>
            </div>
            <div class="col-lg-7 bg-white p-4">
              <h1 class="text-center font-weight-bold text-primary">Registar Utilizador</h1>
              <hr class="my-3" />
              <form action="#" method="post" class="px-3" name="registrar_btn" id="register-form">
                <div class="input-group input-group-lg form-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text rounded-0"><i class="far fa-user fa-lg fa-fw"></i></span>
                  </div>
                  <input type="text" id="nome" name="p_nome" class="form-control rounded-0" placeholder="Primeiro Nome" required />
                </div>
                </div>
                  <input type="text" id="nome" name="Apedilio" class="form-control rounded-0" placeholder="Apedilio" required />
                </div>
                <div class="input-group input-group-lg form-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text rounded-0"><i class="far fa-envelope fa-lg fa-fw"></i></span>
                  </div>
                  <input type="email" id="remail" name="email" class="form-control rounded-0" placeholder="E-Mail" required />
                </div>
                <div class="input-group input-group-lg form-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text rounded-0"><i class="fas fa-key fa-lg fa-fw"></i></span>
                  </div>
                  <input type="password" id="rpassword" name="senha" class="form-control rounded-0" minlength="5" placeholder="Senha" required />
                </div>
                <div class="input-group input-group-lg form-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text rounded-0"><i class="fas fa-key fa-lg fa-fw"></i></span>
                  </div>
                  <input type="password" id="cpassword" name="c_senha" class="form-control rounded-0" minlength="5" placeholder="Confirmar Senha" required />
                </div>
                <div class="form-group">
                  <div id="passError" class="text-danger font-weight-bolder"></div>
                </div>
                <div class="form-group">
                  <input type="submit" id="register-btn" name="registrar_btn" value="Registar.php" class="btn btn-primary btn-lg btn-block botao" />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- Fim d formulario registo -->

      <!-- Form pa redefini senha -->
      <div class="row justify-content-center wrapper" id="forgot-box" style="display: none;">
        <div class="col-lg-10 my-auto sombreados">
          <div class="row">
            <div class="col-lg-7 bg-white p-4">
              <h1 class="text-center font-weight-bold text-primary">Esqueceu da Senha?</h1>
              <hr class="my-3" />
              <p class="lead text-center text-secondary">Para redefinir sua senha, digite o endereço de e-mail registado e enviaremos instruções de redefinição de senha para seu e-mail</p>
              <form action="#" method="post" class="px-3" id="forgot-form">
                <div id="forgotAlert"></div>
                <div class="input-group input-group-lg form-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text rounded-0"><i class="far fa-envelope fa-lg"></i></span>
                  </div>
                  <input type="email" id="femail" name="email" class="form-control rounded-0" placeholder="E-Mail" required />
                </div>
                <div class="form-group">
                  <input type="submit" id="forgot-btn" value="Redefinir Senha" class="btn btn-primary btn-lg btn-block botao" />
                </div>
              </form>
            </div>
            <div class="col-lg-5 d-flex flex-column justify-content-center myColor p-4">
              <h1 class="text-center font-weight-bold text-white">Redefinir Senha!</h1>
              <hr class="my-4 bg-light myHr" />
              <button class="btn btn-outline-light btn-lg font-weight-bolder myLinkBtn align-self-center" id="back-link">Voltar</button>
            </div>
          </div>
        </div>
      </div>
      <!-- fim di Form pa redefini senha -->
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="js/script.js"></script> <!-- undi script sta pa muda form ma mesmo painel -->
  </body>
</html>

<?php
include('includes/footer.php');
?>