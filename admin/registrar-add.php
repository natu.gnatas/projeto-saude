<?php
include('autenticacao.php');
include('includes/header.php');
?>

<div class="container-fluid px-4">
    
    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>Adicionar Utilizador
                    <a href="view-registrar.php" class="btn btn-danger float-end">Voltar</a>
                    </h4>
                </div>
                <div class="card-body">

                    <form action="codigo.php" method="POST" enctype="multipart/form-data">
                        
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="">Nome</label>
                                <input type="text" name="nome" required class="form-control">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="">Apedilio</label>
                                <input type="text" name="Apedilio" required class="form-control">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="">Email</label>
                                <input type="text" name="email" required class="form-control">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="">Senha</label>
                                <input type="password" name="senha" required class="form-control">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label>Confirmar Senha</label>
                                <input required type="password" name="c_senha" placeholder="Confirmar Senha" class="form-control">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="">Telemóvel</label>
                                <input type="number" name="telemovel" required class="form-control">
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="">Foto</label><br>
                                
                                <input type="file" name="foto_utilizador" id="foto_utilizador" class="form-control" required>
                            </div>
                            
                            <div class="col-md-6 mb-3">
                                <label for="">Nível de acesso</label>
                                <select name="role_as" required class="form-control">
                                    <option value="">--Selecionar Função</option>
                                    <option value="1" >Admin</option>
                                    <option value="0" >Utilizador</option>
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="">Estado</label>
                                <input type="checkbox" name="status"  width="70px" height="70px" />
                            </div>
                            <div class="col-md-12 mb-3">
                                <button type="submit" name="Add-Usuarios" class="btn btn-primary">Adicionar</button>
                            </div>

                        </div>
                    </form>

                </div>
            </div>
        </div>

    </div>
</div>

<?php
include('includes/fooder.php');
include('includes/script.php');
?>