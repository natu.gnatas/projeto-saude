<?php
include('autenticacao.php');
include('includes/header.php');
include('configuracao/dbcon.php');
?>

<div class="container-fluid px-4">
    
    <div class="row mt-4">
        <div class="col-md-12">

           <?php include('menssagem.php');?>
           
            <div class="card">
                <div class="card-header">
                    <h4>Editar 
                    <a href="anotacao-view.php" class="btn btn-danger float-end">Voltar</a>
                    </h4>
                </div>
                <div class="card-body">

                    <?php

                    if(isset($_GET['id']))
                    {
                        $post_id = $_GET['id'];
                        $anotacao_editar = "SELECT *FROM anotacao_paciente WHERE id='$post_id' LIMIT 1";
                        $anotacao_run = mysqli_query($conn, $anotacao_editar);

                        if(mysqli_num_rows($anotacao_run) > 0)
                        {
                            $row = mysqli_fetch_array($anotacao_run);
                            ?>
                          
                                <form action="codigo.php" method="POST" enctype="multipart/form-data">
                                    
                                    <input type="hidden" name="post_id" value="<?= $row['id'] ?>">
                                    <div class="row"> 
                                        <div class="col-md-6 mb-3">
                                            <label for="">Nome do Paciente</label>
                                            <input type="text" name="p_nome" value="<?= $row['p_nome'] ?>" required class="form-control">
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Data Nascimento</label>
                                            <input type="date" name="idade" value="<?= $row['idade'] ?>" required class="form-control">
                                        </div>
                                        <div class="col-md-6 mb-3">
                                        <label for="">Sexo</label><br>
                                            
                                        <input type="radio" name="sexo" value="<?= $row['sexo'] ?>" required>Masculino<br>
                                        <input type="radio" name="sexo" value="<?= $row['sexo'] ?>" required>Feminino<br>
                                        <input type="radio" name="sexo" value="<?= $row['sexo'] ?>" required>Não declarado <br>

                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label for="">Foto</label><br>
                                            <input type="hidden" name="old_foto" value="<?= $row['foto_paciente'] ?>">
                                                <input type="file" name="foto_paciente" class="form-control" required>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label for="">Profissional de saúde responsável</label>
                                          <select name="responsavel" required>
                                            <option value="">Selecione o profissional de saúde responsável</option>
                                            <?php 
                                                
                                            
                                                $user_id= "SELECT id, nome FROM users";
                                                $user_query =mysqli_query($conn, $user_id);
                                                while($row_user_id = mysqli_fetch_assoc($user_query)){?>
                                                <option value="<?php echo $row_user_id['nome']; ?>"><?php echo $row_user_id['nome'] ?></option><?php

                                                }
                                            
                                            ?>
                                 
                                         </select>
                                            
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Telemóvel</label>
                                            <input type="number" name="telemovel" value="<?= $row['telemovel'] ?>" required class="form-control">
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">BI</label>
                                            <input type="number" name="BI" value="<?= $row['BI'] ?>" required class="form-control">
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label for="">Descrição</label>
                                            <textarea name="descricao" id="nota_descricao" class="form-control" rows="4"><?= $row['descricao'] ?></textarea>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Histórico</label>
                                            <textarea name="historico" id="nota_historico" class="form-control" rows="4"><?= $row['historico'] ?></textarea>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Estado Navegação</label>
                                            <input type="checkbox" name="navbar_status" <?= $row['navbar_status']== '1' ? 'checked':'' ?> width="70px" height="70px" />
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Estado</label>
                                            <input type="checkbox" name="status" <?= $row['status']== '1' ? 'checked':'' ?>  width="70px" height="70px" />
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <button type="submit" name="atualizar_nota" class="btn btn-primary">Atualizar</button>
                                        </div>

                                    </div>
                                </form>

                           <?php
                        }
                        else
                        {
                           ?>
                           <h4>Nenhuma gravação encontrada</h4>
                           <?php
                        }
                    }
                  
                    ?>

                </div>
            </div>
        </div>

    </div>
</div>

<?php
include('includes/fooder.php');
include('includes/script.php');
?>