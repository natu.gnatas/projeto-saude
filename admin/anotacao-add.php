<?php
include('autenticacao.php');
include('includes/header.php');
include('configuracao/dbcon.php');
?>

<div class="container-fluid px-4">
    
    <div class="row mt-4">
        <div class="col-md-12">

           <?php include('menssagem.php');?>
            <div class="card">
                <div class="card-header">
                    <h4><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                    <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                    </svg>
                    <a href="anotacao-view.php" class="btn btn-danger float-end">Voltar</a>
                    </h4>
                </div>
                <div class="card-body">
                   
                    <form action="codigo.php" method="POST" enctype="multipart/form-data">
                        
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="">Nome do Paciente</label>
                                <input type="text" name="p_nome" required class="form-control">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="">Data Nascimento</label>
                                <input type="date" name="idade"  required class="form-control">
                            </div>
                           
                            <div class="col-md-6 mb-3">
                                <label for="">Sexo</label><br>
                                
                                <input type="radio" name="sexo" value="Masculino" required>Masculino<br>
                                <input type="radio" name="sexo" value="Feminino" required>Feminino<br>
                                <input type="radio" name="sexo" value="Não declarado" required>Não declarado <br>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="">Foto</label><br>
                                
                                <input type="file" name="foto_paciente" id="foto_paciente" class="form-control" required>
                            </div>
                           
                            <div class="col-md-6 mb-3">
                               <label for="">Profissional de saúde responsável</label><br>
                               
                                 <select name="responsavel" required>
                                   <option value="<?php echo $row_user_id['id']; ?>">Selecione o profissional de saúde responsável</option>
                                   <?php 
                                    
                                    $user_id= "SELECT id, nome FROM users";
                                    $user_query =mysqli_query($conn, $user_id);
                                    while($row_user_id = mysqli_fetch_assoc($user_query)){?>
                                    <option value="<?php echo $row_user_id['nome']; ?>"><?php echo $row_user_id['nome'] ?></option><?php

                                    }
                                
                                   ?>
                                 
                                 </select>
                            
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="">Telemóvel</label>
                                <input type="number" name="telemovel"  required class="form-control">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="">BI</label>
                                <input type="number" name="BI" class="form-control" required>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="">Descrição</label>
                                <textarea name="descricao" id="nota_descricao" class="form-control" rows="4" required></textarea>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="">Histórico</label>
                                <textarea name="historico" id="nota_historico" class="form-control" rows="4" required></textarea>
                            </div>
                            <!--<div class="col-md-6 mb-3">
                                <label for="">status Navegação</label>
                                <input type="checkbox" name="navbar_status"  width="70px" height="70px" />
                            </div>-->
                            <div class="col-md-6 mb-3">
                                <label for="">Estado</label>
                                <input type="checkbox" name="status"  width="70px" height="70px" />
                            </div>
                            
                            <div class="col-md-12 mb-3">
                                <button type="submit" name="add_nota" class="btn btn-primary">Guardar</button>
                            </div>

                        </div>
                    </form>

                </div>
            </div>
        </div>

    </div>
</div>

<?php
include('includes/fooder.php');
include('includes/script.php');
?>