<?php
session_start();
include('autenticacao.php');


if(isset($_POST['eliminar_anotacao']))
{
   $anotacao_id =mysqli_real_escape_string($conn, $_POST['anotacao_id']);
   $anotacao_query = "SELECT * FROM anotacao_paciente WHERE id='$anotacao_id'";
   $anotacao_query_run = mysqli_query($conn, $anotacao_query);
   $anotacao_data= mysqli_fetch_array($anotacao_query_run);

   $query = "DELETE FROM anotacao_paciente WHERE id='$anotacao_id'";
   $query_run = mysqli_query($conn, $query);

   if($query_run)
   {
      if(file_exists($anotacao_id))
      {
         unlink($anotacao_id);
      }
      echo 200;
      $_SESSION['menssagem'] = "Eliminado com sucesso";
      header('Location: anotacao-view.php?id'.$anotacao_id);
      exit(0);
   }
   else
   {
      echo 500;
     $_SESSION['menssagem'] = "Algo correu mal.!";
      header('Location: anotacao-view.php');
      exit(0);
      
   }
   
}

if(isset($_POST['atualizar_nota']))
{
   $post_id = $_POST['post_id'];
   $anotacao_id = $_POST['anotacao_id'];
   $p_nome = $_POST['p_nome'];
   $idade = $_POST['idade'];
   $sexo = $_POST['sexo'];
   $responsavel = $_POST['responsavel'];
   $telemovel = $_POST['telemovel'];
   $BI = $_POST['BI'];
   $descricao = $_POST['descricao'];
   $historico = $_POST['historico'];
   $navbar_status = $_POST['navbar_status '] == true ? '1':'0';
   
   $old_filename = $_POST['old_foto'];
   $foto_paciente = $_FILES['foto_paciente']['p_nome'];

   $update_filename = "";
   if($foto_paciente != NULL)
   {
     $foto_extension = pathinfo($foto_paciente, PATHINFO_EXTENSION);
     $filename = time().'.jpg'.$foto_extension;

     $update_filename = $filename;
   }
   else
   {
      $update_filename = $old_filename;
   }
   $status = $_POST['status '] == true ? '1':'0';

   $query = "UPDATE anotacao_paciente SET anotacao_id='$anotacao_id', p_nome='$p_nome', idade='$idade', sexo='$sexo', foto_paciente='$update_filename',responsavel='$responsavel',telemovel='$telemovel',BI='$BI',descricao='$descricao', historico='$historico', navbar_status='$navbar_status', 
   status='$status' WHERE id='$post_id' ";
  
  $query_run = mysqli_query($conn, $query);

  if($query_run)
   {
      if($foto_paciente != NULL)
      {
        if(file_exists('../uploads/fotos/'.$old_filename))
        {
          unlink("../uploads/fotos/".$old_filename);
        } 
        move_uploaded_file($_FILES['foto_paciente']['tmp_name'], '../uploads/fotos/'.$update_filename);
      }
      $_SESSION['menssagem'] = "Atualizado com sucesso";
      header('Location: anotacao-view.php?id='.$post_id);
      exit(0);
   }
   else
   {
     $_SESSION['menssagem'] = "Algo correu mal.!";
      header('Location: anotacao-view.php?id='.$post_id);
      exit(0);
   }

}

if(isset($_POST['add_nota']))
{
   $anotacao_id = $_POST['anotacao_id'];
   $p_nome = $_POST['p_nome'];
   $idade = $_POST['idade'];
   $sexo = $_POST['sexo'];
   $responsavel = $_POST['responsavel'];
   $telemovel = $_POST['telemovel'];
   $BI = $_POST['BI'];
   $descricao = $_POST['descricao'];
   $historico = $_POST['historico'];
   $navbar_status = $_POST['navbar_status '] == true ? '1':'0';
   

   $foto_paciente = $_FILES['foto_paciente']['p_nome'];
   $foto_extension = pathinfo($foto_paciente, PATHINFO_EXTENSION);
   $filename = time().'.jpg'.$foto_extension;

   $status = $_POST['status '] == true ? '1':'0';
   
   $query = "INSERT INTO anotacao_paciente (anotacao_id,p_nome,idade,sexo,foto_paciente,responsavel,telemovel,BI,descricao,historico,navbar_status,status) VALUE ('$anotacao_id','$p_nome','$idade','$sexo','$filename','$responsavel','$telemovel','$BI','$descricao','$historico','$navbar_status','$status')";


   $query_run = mysqli_query($conn, $query);

   if($query_run)
   {
      move_uploaded_file($_FILES['foto_paciente']['tmp_name'], '../uploads/fotos/'.$filename);
      $_SESSION['menssagem'] = "Paciente Adicionado com sucesso";
      header('Location: anotacao-view.php');
      exit(0);
   }
   else
   {
     $_SESSION['menssagem'] = "Algo correu mal!";
      header('Location: anotacao-add.php');
      exit(0);
   }
}

if(isset($_POST['eliminar-usuario']))
{
   $user_id =mysqli_real_escape_string($conn, $_POST['user_id']);
   $user_query = "SELECT * FROM users WHERE id='$user_id'";
   $user_query_run = mysqli_query($conn, $user_query);
   $user_data= mysqli_fetch_array($user_query_run);

   $query = "DELETE FROM users WHERE id='$user_id'";
   $query_run = mysqli_query($conn, $query);

   if($query_run)
   {
      if(file_exists($user_id))
      {
         unlink($user_id);
      }
      echo 200;
      $_SESSION['menssagem'] = "Utilizador eliminado com sucesso";
      header('Location: view-registrar.php'.$user_id);
      exit(0);
   }
   else
   {
      echo 500;
     $_SESSION['menssagem'] = "Algo correu mal.!";
      header('Location: view-registrar.php'.$user_id);
      exit(0);
      
   }
   
}
if(isset($_POST['Add-Usuarios']))
{
    $nome = $_POST['nome'];
    $Apedilio = $_POST['Apedilio'];
    $email = $_POST['email'];
    $senha = $_POST['senha'];
    $senha = md5($senha);
    $c_senha = $_POST['c_senha'];
    $c_senha =md5($c_senha);
    $telemovel = $_POST['telemovel'];
    $role_as = $_POST['role_as'];
       
    $foto_utilizador = $_FILES['foto_utilizador']['nome'];
    $foto_extension2 = pathinfo($foto_utilizador, PATHINFO_EXTENSION);
    $filename2 = time().'.jpg'.$foto_extension2;

    $status = $_POST['status '] == true ? '1':'0';

     if($senha == $c_senha)
     {
            //verificar email
            $verificar_email = "SELECT email FROM users WHERE email='$email'";
            $verificar_email_run = mysqli_query($conn, $verificar_email);

            if(mysqli_num_rows($verificar_email_run) > 0)
            {
               //email já existe
               $_SESSION['menssagem'] = "Esse email já existe";
               header("Location: registrar-add.php");
               exit(0);
            }
         else
         {
            $query = "INSERT INTO users (nome, Apedilio, email, senha,telemovel,foto_utilizador, role_as, status) VALUES ('$nome', '$Apedilio', '$email', '$senha','$telemovel','$filename2', '$role_as', '$status')";

             $query_run = mysqli_query($conn, $query);

         
            if($query_run)
            {
               move_uploaded_file($_FILES['foto_utilizador']['tmp_name'], '../uploads2/fotos2/'.$filename2);
               $_SESSION['menssagem'] = "Utilizador Adicionado com sucesso";
               header('Location: view-registrar.php');
               exit(0);
            }
            else
            {
               $_SESSION['menssagem'] = "Algo correu mal.!";
               header('Location: view-registrar.php');
               exit(0);
            }
         }

         
     }else
     {
      $_SESSION['menssagem'] = "Senha introduzida não correspondente";
        header("Location: registrar-add.php");
        exit(0);
     }
}

if(isset($_POST['atualizar-usuarios']))
{
  $user_id = $_POST['user_id'];
  $nome = $_POST['nome'];
  $Apedilio = $_POST['Apedilio'];
  $email = $_POST['email'];
  $senha = $_POST['senha'];
  $senha = md5($senha);
  $telemovel = $_POST['telemovel'];
  $role_as = $_POST['role_as'];
  
  $old_filename2 = $_POST['old_foto2'];
  $foto_utilizador = $_FILES['foto_utilizador']['nome'];

   $update_filename2 = "";
   if($foto_utilizador != NULL)
   {
     $foto_extension2 = pathinfo($foto_utilizador, PATHINFO_EXTENSION);
     $filename2 = time().'.jpg'.$foto_extension2;

     $update_filename2 = $filename2;
   }
   else
   {
      $update_filename2 = $old_filename2;
   }
   $status = $_POST['status '] == true ? '1':'0';

  $query = "UPDATE users SET nome='$nome', Apedilio='$Apedilio', email='$email', senha='$senha',telemovel='$telemovel',foto_utilizador='$update_filename2', role_as='$role_as', status='$status' 
               WHERE id='$user_id'";

  $query_run = mysqli_query($conn, $query);

  if($query_run)
  {
      if($foto_utilizador != NULL)
      {
         if(file_exists('../uploads2/fotos2/'.$old_filename2))
         {
            unlink("../uploads2/fotos2/".$old_filename2);
         } 
         move_uploaded_file($_FILES['foto_utilizador']['tmp_name'], '../uploads2/fotos2/'.$update_filename2);
      }
      $_SESSION['menssagem'] = 'Atualizado com sucesso';
      header('Location: view-registrar.php?id='.$user_id);
      exit(0);
   }
   else
   {
     $_SESSION['menssagem'] = "Algo correu mal.!";
      header('Location: view-registrar.php?id='.$user_id);
      exit(0);
   }
}

?>