<?php
include('autenticacao.php');
include('includes/header.php');
//include('include/configuracao/dbcon.php');

?>

 <!-- Favicon icon -->
      <link rel="icon" href="assets2/images/favicon.ico" type="image/x-icon">
      <!-- Google font-->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
      <!-- Required Fremwork -->
      <link rel="stylesheet" type="text/css" href="assets2/css/bootstrap/css/bootstrap.min.css">
      <!-- themify-icons line icon -->
      <link rel="stylesheet" type="text/css" href="assets2/icon/themify-icons/themify-icons.css">
      <!-- ico font -->
      <link rel="stylesheet" type="text/css" href="assets2/icon/icofont/css/icofont.css">
      <!-- Style.css -->
      <link rel="stylesheet" type="text/css" href="assets2/css/style.css">
      <link rel="stylesheet" type="text/css" href="assets2/css/jquery.mCustomScrollbar.css">

      <!-- fontawesome css -->
     <!-- <link rel="stylesheet" type="text/css" href="admin/css/fontawesome.css">-->

<div class="container-fluid px-4">
    <!--<h1 class="mt-4"> <strong>Acompanhe o seu paciente em tempo real</strong> </h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"></li>
    </ol>-->
    <div class="row">
       <div class="col-md-6 col-xl-3 ">
            <div class="card widget-card-1">
                <div class="card-block-small">
                   <i class="icofont icofont-users-alt-1 bg-c-blue card1-icon"></i>
                    <span class="text-c-blue f-w-600">Utilizadores</span>
                    <?php
                        $dash_cadastrado_query = "SELECT * from users";
                        $dash_cadastrado_query_run = mysqli_query($conn, $dash_cadastrado_query);

                        if($total_cadastrado = mysqli_num_rows($dash_cadastrado_query_run))
                        {
                           echo '<h4 class="mb-0"> '.$total_cadastrado.' </h4>';
                        }
                        else
                        {
                           echo '<h4 class="mb-0"> Não há Usúarios Registrados </h4>';
                        }
                    ?>
                    <div>
                        <span class="f-left m-t-10 text-muted">
                            <i class="text-c-blue f-16 icofont icofont-user m-r-10"></i>Admin/Utilizador
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3">
            <div class="card widget-card-1 ">
                <div class="card-block-small">
                    <i class="icofont icofont-warning-alt bg-c-pink card1-icon"></i>
                    
                    <span class="text-c-pink f-w-600">Alerta de Perigo</span>
                    <?php
                            $dash_alerta_query = "SELECT * FROM alerta";
                            $dash_alerta_query_run = mysqli_query($conn, $dash_alerta_query);

                            if($total_alerta = mysqli_num_rows($dash_alerta_query_run))
                            {
                            echo '<h4 class="mb-0"> '.$total_alerta.' </h4>';
                            }
                            else
                            {
                            echo '<h4 class="mb-0"> Não há alertas </h4>';
                            }
                        ?>
                    <div>
                        <span class="f-left m-t-10 text-muted">
                            <i class="text-c-info f-16 icofont icofont-warning m-r-10" ></i>Pacientes em perigo
                            
                        </span>
                    </div>
                </div>
            </div>
        </div>
        
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-notepad bg-c-green card1-icon"></i>
                        <span class="text-c-green f-w-600">Notas do paciente</span>
                        <?php
                            $dash_anotacao_query = "SELECT * from anotacao_paciente";
                            $dash_anotacao_query_run = mysqli_query($conn, $dash_anotacao_query);

                            if($total_anotacao = mysqli_num_rows($dash_anotacao_query_run))
                            {
                            echo '<h4 class="mb-0"> '.$total_anotacao.' </h4>';
                            }
                            else
                            {
                            echo '<h4 class="mb-0"> Não há Notas Registrados </h4>';
                            }
                        ?>
                        <div>
                            <span class="f-left m-t-10 text-muted">
                                <i class="text-c-green f-16 icofont icofont-tag m-r-10"></i>Dados importante 
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
        
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                    <i class="icofont icofont-ui-user-group bg-c-yellow card1-icon"></i>
                        <span class="text-c-yellow f-w-600 ">Pacientes </span>
                            <?php
                                $dash_pacientes_query = "SELECT p_nome from anotacao_paciente ORDER BY id DESC";
                                $dash_pacientes_query_run = mysqli_query($conn, $dash_pacientes_query);

                                if($total_pacientes = mysqli_num_rows($dash_pacientes_query_run))
                                {
                                echo '<h4 class="mb-0"> '.$total_pacientes.' </h4>';
                                }
                                else
                                {
                                echo '<h4 class="mb-0"> Não há Pacientes Registrados </h4>';
                                }
                            ?>
                        <div>
                            <span class="f-left m-t-10 text-muted">
                                <i class="text-c-yellow f-16 icofont icofont-patient-file m-r-10"></i>Pacientes em consulta
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->

    </div>
</div>

<?php
include('includes/fooder.php');
include('includes/script.php');
?>