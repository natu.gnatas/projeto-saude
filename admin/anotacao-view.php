<?php
include('autenticacao.php');
include('includes/header.php');
?>
<style>
    .table-bg{
        background: rbg(0,0,0,0.3);
        border-radius: 15px 15px 0 0;
    }
    .border_foto
    {
        border-radius: 70px 70px 70px 70px;
    }
</style>

<div class="container-fluid px-4">
    
    <div class="row mt-4">
        <div class="col-md-12">

           <?php include('menssagem.php');?>

            <div class="card">
                <div style="background:#bdc0c2"; class="card-header border-dark border-3" >
                    <h4>Pacientes Registrados
                    
                        <!-------------------------------------------Baixar Notas dos pacientes------------------------------------------------->
                            
                        <button  href="" class="btn btn-info float-end" id="baixar" style="margin: 0 15px;"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-download" viewBox="0 0 16 16">
                        <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                        <path d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"/>
                        </svg></button>

                        <a href="anotacao-add.php" class="btn btn-primary float-end"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                        </svg></a>

                                                
                    </h4>
                </div>
                <div class="card-body" id="anotacoes" >

                     <div class="table-responsive">
                           <table id="tabela" class="table table table-bordered table-stripe border-dark border-3 table-bg">
                              <thead style="background:#bdc0c2";>
                                  <tr>
                                      <th>ID</th>
                                      <th>Nome</th>
                                      <th>Data Nascimento</th>
                                      <th>Sexo</th>
                                      <th>Foto</th>
                                     <!-- <th>Estado</th>-->
                                      <th>Responsável</th>
                                      <th>Telemóvel</th>
                                      <th>Nº BI</th>
                                      <th>Descrição</th>
                                      <th>Histórico</th>
                                      <th>Editar</th>
                                      <th>Eliminar</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <?php 
                                    $anotacao = "SELECT *FROM anotacao_paciente WHERE status!='2' ";
                                    $anotacao_run = mysqli_query($conn, $anotacao);

                                    if(mysqli_num_rows($anotacao_run) > 0)
                                    {
                                        foreach($anotacao_run as $item)
                                        {
                                            ?>
                                            <tr>
                                                <td class="anotacao_id"><?= $item['id']; ?></td>
                                                <td><?= $item['p_nome'] ?></td>
                                                <td><?= $item['idade'] ?></td>
                                                <td><?= $item['sexo'] ?></td>
                    <td><img  src="../uploads/fotos/<?= $item['foto_paciente'] ?>" width="120px" height="120px" class="border_foto" alt=""/></td>
                                                
                                                <td><?= $item['responsavel'] ?></td>
                                                <td><?= $item['telemovel'] ?></td>
                                                <td><?= $item['BI'] ?></td>
                                                <td><?= $item['descricao'] ?></td>
                                                <td><?= $item['historico'] ?></td>

                                                <!-------------------------Editar Notas dos pacientes----------------------------------------->
                                                <td>
                                                    <a href="anotacao-editar.php?id=<?= $item['id']?>" class="btn btn-warning"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                                    </svg></a>
                                                </td>

                                                <!-------------------------Eliminar Notas dos pacientes----------------------------------------->
                                                <td>
                                                    <form action="codigo.php?id=<?= $item['id']; ?>" method="POST">
                                                    <button type="submit" name="eliminar_anotacao"  value="<?=$item['id'];?>" class="eliminar_anotacao btn btn-danger "><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                                                    <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z"/>
                                                    </svg></button>
                                                    </form> 
                                                </td>

                                            </tr>
                                            <?php
                                        }
                                    }
                                    else
                                    {
                                        ?>
                                            <tr>
                                                <td colspan="13">Nenhuma gravação encontrada</td>
                                            </tr>
                                        <?php
                                    }
                               ?>

                             <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                             <script src="assets/css/bootstrap/js/bootstrap.min.js"></script>
                             <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                             <script src="sweetalert2.min.js"></script>
                             <link rel="stylesheet" href="css/sweetalert2.min.css">  
                             <script src="assets/swal2/sweetalert2.min.js"></script>

                             <!--baixar em formato pdf as informações dos pacientes -->
                             <script src="js/html2pdf.bundle.min.js"></script>
                                <script type="text/javascript" >

                                   document.getElementById('baixar').onclick = function(){
                                    var element = document.getElementById('tabela');

                                    var opt= {
                                        margin:       0.8,
                                        filename:     'Ficheiro Paciente.pdf',
                                        image:        { type: 'jpeg', quality: 1},
                                        html2canvas:  { scale: 2 },
                                        jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape'}
                                    };
                                     html2pdf(element, opt);
                                   };

                                </script>
                              <!--fim de baixar em formato pdf as informações dos pacientes -->
                               
                              
                                <!--confirmar se realmente quer iliminar informações dos pacientes -->
                                <script>
                                    $(document).ready(function(){
                                        
                                        
                                        $('.eliminar_anotacao').click(function(e){
                                            
                                            e.preventDefault();
                                            var id = $(this).val();

                                            Swal.fire({
                                                title: 'Tens certeza que desejas eliminar essa Nota?',
                                                text: "Você não poderá reverter isso!",
                                                icon: 'warning',
                                                showCancelButton: true,
                                                confirmButtonColor: '#3085d6',
                                                cancelButtonColor: '#d33',
                                                confirmButtonText: 'Sim, Eliminar!'
                                            }).then((result) => {

                                                        window.location.reload();
                                                  
                                            if (result.isConfirmed) 
                                            {
                                                window.location.reload();
                                                $.ajax({
                                                    method: "POST",
                                                    url: "codigo.php",
                                                    data:{
                                                       'anotacao_id':id,
                                                        'eliminar_anotacao':true
                                                    },
                                                    success: function(response){
                                                        if(response == 200)
                                                        {
                                                           
                                                            Swal.fire('success!','Paciente eliminado com sucesso!','success');
                                                            $("#anotacoes").load(location.href + " #anotacoes");
                                                        }
                                                        else if(response == 500)
                                                        {
                                                            swal.fire("Erro!", "Algo correu mal!", "Error");
                                                        }
                                                    }
                                                });
                                            }
                                            else
                                            {
                                                swal.fire("O ficheiro está salvo!");
                                            }
                                            });
                                        });
                                        
                                    });
                                    
                                                                 
                                </script>

                                  <!--fim confirmar se realmente quer iliminar Notas -->

                              </tbody>
                         </table>
                     </div>
                
                </div>
            </div>
        </div>

    </div>
</div>

<?php
include('includes/fooder.php');
include('includes/script.php');
?>