<?php

session_start();
/*
if(isset($_SESSION['auth']))
{
    $_SESSION['menssagem'] = "Você já está conectado";
    header("Location: index.php");
    exit(0);
}*/

include('includes/header.php');
include('includes/navbar.php');
?>

<div class="py-5">
    <div class="container">
        <div class="row justify-content-center ">
            <div class="col-md-5">

              <?php include('menssagem.php');?>
              
                <div class="card">
                    <div class="card-header">
                        <h4>Registrar</h4>
                    </div>
                    <div class="card-body">

                        <form action="code_registrar.php" method="POST">
                            <div class="form-group-mb-3">
                                <label>Nome</label>
                                <input required type="text" name="nome"  placeholder="Degite o primeiro Nome" class="form-control">
                            </div>
                            <div class="form-group-mb-3">
                                <label>Apedilio</label>
                                <input required type="text" name="Apedilio"  placeholder="Degite o ultimo apedilio " class="form-control">
                            </div>
                            <div class="form-group-mb-3">
                                <label>Email </label>
                                <input required type="email" name="email" placeholder="Degite o email" class="form-control">
                            </div>
                            <div class="form-group-mb-3">
                                <label>Password</label>
                                <input required type="password" name="senha"  placeholder="Degite a Senha" class="form-control">
                            </div>
                            <div class="form-group-mb-3">
                                <label>Confirmar Password</label>
                                <input required type="password" name="c_senha" placeholder="Confirmar Senha" class="form-control">
                            </div>
                            <div class="form-group-mb-3">
                                <button required type="submit" name= "Add-Usuarios" class="btn btn-primary">Registrar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<?php
include('includes/footer.php');
?>