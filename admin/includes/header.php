<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="Emanuel Miranda" content="" />
        <title>Patient Monitoring</title>

        <link href="css/styles.css" rel="stylesheet" />
        <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
        <link href="css/dataTables.bootstrap5.min.css" rel="stylesheet" />

        <!-- summernote CDN link-->
        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
       <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        
    </head>

    <body class="sb-nav-fixed">

    <?php include('includes/navbar-top.php');?>

    <div id="layoutSidenav"> 

    <?php include('includes/sidebar.php'); ?>

    <div id="layoutSidenav_content">
    <main>