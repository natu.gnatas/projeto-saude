<?php
 session_start();
?>

<link rel="stylesheet" type="text/css" href="css/notificason.css"></link>

<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
    <!-- Navbar Brand-->
    <a class="navbar-brand ps-3 text-primary border-3 border-primary" href="cliente.php">Patient Monitoring</a>
    <!-- Sidebar Toggle-->
    <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
    
    <!-- Navbar Search-->
    <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
     
    <div class="input-group">
                    
                                <!-- Notificação-->
        
                <!-- Alerta dos valores criticos-->
                <li class="nav-item dropdown no-arrow mx-1">
                            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-bell fa-fw"></i>
                                <!-- Counter - Alerts -->
                                <span class="badge badge-danger bg-danger" id="contar" ><?php echo mysqli_num_rows(mysqli_query($conn,"SELECT * FROM alerta WHERE view=1")) ?></span>
                            </a>
                            <!-- Dropdown - Alerts -->
                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in list" aria-labelledby="alertsDropdown">
                                <h6 class="dropdown-header">
                                Alertas do Sistema
                                </h6>

                                <?php
                                $res=mysqli_query($conn,"SELECT * FROM alerta ORDER BY data DESC LIMIT 5");
                                if($res){
                                    while ($linha=mysqli_fetch_assoc($res)) {
                                    echo '<a class="dropdown-item d-flex align-items-center" href="cliente.php">
                                        <div class="mr-3 msg">
                                            <div class="icon-circle bg-warning">
                                            <i class="fas fa-exclamation-triangle text-white"></i>
                                            </div>
                                        </div>
                                        <div class="text-primary msg" >
                                            <div class="small text-gray-500 text-dark">'.$linha['data'].'</div> 
                                            '.$linha['menssagem']. '
                                        </div>
                                        </a>';
                                    }
              
                                }

                                ?>
                                <a class="dropdown-item text-center small text-gray-500" href="dados.php">Ver todas</a>

                                <script type="text/javascript">
        
                                </script>
                                
                            </div>
                        </li>
                                <!--fim de notificação-->
    
      </div>
    

    </form>
    <!-- Navbar-->
    <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
        <li class="nav-item dropdown">
            <!--<a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>-->
            
            <?php if (isset($_SESSION['auth_user'])) : ?>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              <?= $_SESSION['auth_user']['user_name']; ?>
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="#"> <strong>Meu Perfil</strong> </a></li>
              <li>
                <form action="../todocodigo.php" method="POST">
                  <button type="submit" name="sair_btn" class="dropdown-item">Sair</button>
                </form>
              </li>
            </ul>
          </li>
        <?php else : ?>
        
          <li class="nav-item">
            <a class="nav-link" href="../index.php"> Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../registrar-add.php">Registrar</a>
          </li>
        
        <?php endif; ?>

            
           <!-- <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#!">Configurações</a></li>
             <li><a class="dropdown-item" href="#!"></a></li>
                <li>
                    <hr class="dropdown-divider" />
                </li>
                <li><a class="dropdown-item" href="../todocodigo.php">Sair</a></li>
            </ul>-->
        </li>
    </ul>

</nav>
  <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    