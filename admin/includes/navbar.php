<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"> <strong> Patient Monitoring</strong></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="./admin/utilizador2.php"> <strong>Utilizador</strong> </a>
        </li>
        <li class="nav-item">
          <!--<a class="nav-link" href="#">Link</a>-->
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          <i class="fas fa-user fa-fw"></i> 
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="#"> <strong>Pagina</strong> </a></li>
            <li><a class="dropdown-item" href="#"> <strong>Outros</strong> </a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="../index.php"> <strong>Sair</strong> </a></li>
          </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php"> <strong>Login</strong></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="registrar-add.php"> <strong>Registrar</strong> 
              
          </a>
        </li>
      </ul>
      
    </div>
  </div>
</nav>