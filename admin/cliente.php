<?php
include('autenticacao.php');
include('includes/header.php');


//calcular Temperatura maximo e minimo
$query = "SELECT MAX(value1) as maxvalue1 from sensordata";
$res=mysqli_query($conn,$query);
$value1Max=mysqli_fetch_array($res);
$value1Alto = $value1Max['maxvalue1'];
// echo $value1Alto;

$query = "SELECT MIN(value1) as minvalue1 from sensordata";
$res=mysqli_query($conn,$query);
$value1Min=mysqli_fetch_array($res);
$value1Baixo = $value1Min['minvalue1'];
// echo $value1Baixo;

//calcular Batimento Cardiáco maximo e minimo
$query = "SELECT MAX(value2) as maxvalue2 from sensordata";
$res=mysqli_query($conn,$query);
$value2Max=mysqli_fetch_array($res);
$value2Alto = $value2Max['maxvalue2'];
// echo $value2Alto;

$query = "SELECT MIN(value2) as minvalue2 from sensordata";
$res=mysqli_query($conn,$query);
$value2Min=mysqli_fetch_array($res);
$value2Baixo = $value2Min['minvalue2'];
// echo $value2Baixo;

//calcular Pressão Arterial sistólica maximo e minimo
$query = "SELECT MAX(value3) as maxvalue3 from sensordata";
$res=mysqli_query($conn,$query);
$value3Max=mysqli_fetch_array($res);
$value3Alto = $value3Max['maxvalue3'];
// echo $value3Alto;

$query = "SELECT MIN(value3) as minvalue3 from sensordata";
$res=mysqli_query($conn,$query);
$value3Min=mysqli_fetch_array($res);
$value3Baixo = $value3Min['minvalue3'];
// echo $value3Baixo;

//calcular Pressão Arterial Diastólica maximo e minimo
$query = "SELECT MAX(value4) as maxvalue4 from sensordata";
$res=mysqli_query($conn,$query);
$value4Max=mysqli_fetch_array($res);
$value4Alto = $value4Max['maxvalue4'];
// echo $value3Alto;

$query = "SELECT MIN(value4) as minvalue4 from sensordata";
$res=mysqli_query($conn,$query);
$value4Min=mysqli_fetch_array($res);
$value4Baixo = $value4Min['minvalue4'];
// echo $value3Baixo;

//calcular Frequência Respiratória maximo e minimo
$query = "SELECT MAX(value5) as maxvalue5 from sensordata";
$res=mysqli_query($conn,$query);
$value5Max=mysqli_fetch_array($res);
$value5Alto = $value5Max['maxvalue5'];
// echo $value4Alto;

$query = "SELECT MIN(value5) as minvalue5 from sensordata";
$res=mysqli_query($conn,$query);
$value5Min=mysqli_fetch_array($res);
$value5Baixo = $value5Min['minvalue5'];
// echo $value4Baixo;

?>

<div class="container-fluid px-4">
    
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item active"></li>
    </ol>
    <div class="row">
        <!-- temperatura-->
        <div class="col-xl-3 col-md-6">
            <div class="card bg-light border-5 border-primary text-dark mb-4">
                <div class="card-body"><strong>Temperatura</strong> <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-thermometer-low text-primary" viewBox="0 0 16 16">
                <path d="M9.5 12.5a1.5 1.5 0 1 1-2-1.415V9.5a.5.5 0 0 1 1 0v1.585a1.5 1.5 0 0 1 1 1.415z"/>
                <path d="M5.5 2.5a2.5 2.5 0 0 1 5 0v7.55a3.5 3.5 0 1 1-5 0V2.5zM8 1a1.5 1.5 0 0 0-1.5 1.5v7.987l-.167.15a2.5 2.5 0 1 0 3.333 0l-.166-.15V2.5A1.5 1.5 0 0 0 8 1z"/>
                </svg>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                    </div>
                    <h6 class="mb-0"> Máximo: <?php echo $value1Alto;?> °C</h6>
                    <h6 class="mb-0"> Mínimo: <?php echo $value1Baixo;?> °C</h6>
                    <i class="fa-thin fa-droplet-degree"></i>
                </div>
               
            </div>
        </div>

        <!--batimento cardiaco -->
        <div class="col-xl-3 col-md-6">
            <div class="card bg-light border-5 border-primary text-dark mb-4">
                
                <div class="card-body"><strong>Batimento Cardiáco</strong>  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-heart-pulse-fill text-danger" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M1.475 9C2.702 10.84 4.779 12.871 8 15c3.221-2.129 5.298-4.16 6.525-6H12a.5.5 0 0 1-.464-.314l-1.457-3.642-1.598 5.593a.5.5 0 0 1-.945.049L5.889 6.568l-1.473 2.21A.5.5 0 0 1 4 9H1.475ZM.879 8C-2.426 1.68 4.41-2 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C11.59-2 18.426 1.68 15.12 8h-2.783l-1.874-4.686a.5.5 0 0 0-.945.049L7.921 8.956 6.464 5.314a.5.5 0 0 0-.88-.091L3.732 8H.88Z"/>
                </svg>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                    </div>
                    <h6 class="mb-0"> Máximo: <?php echo $value2Alto;?> bpm</h6>
                    <h6 class="mb-0"> Mínimo: <?php echo $value2Baixo;?> bpm</h6>
                </div>
                
            </div>
        </div>
          <!--Pressão Artirial Sistólica -->
        <div class="col-xl-3 col-md-6">
            <div class="card bg-light border-5 border-primary text-dark mb-4">

                <div class="card-body"><strong>Pressão Arterial Sistólica</strong> 
                    <div class="card-footer d-flex align-items-center justify-content-between">
                    </div>
                    <h6 class="mb-0"> Máximo : <?php echo $value3Alto;?> mmHg</h6>
                    <h6 class="mb-0"> Mínimo: <?php echo $value3Baixo;?> mmHg</h6>
                    
                </div>
                
            </div>
        </div>
         <!--Pressão Artirial Diastólica -->
         <div class="col-xl-3 col-md-6">
            <div class="card bg-light border-5 border-primary text-dark mb-4">

                <div class="card-body"><strong>Pressão Arterial Diastólica</strong> 
                    <div class="card-footer d-flex align-items-center justify-content-between">
                    </div>
                    <h6 class="mb-0"> Máximo : <?php echo $value4Alto;?> mmHg</h6>
                    <h6 class="mb-0"> Mínimo: <?php echo $value4Baixo;?> mmHg</h6>
                    
                </div>
                
            </div>
        </div>
          <!--Frequencia Respiratoria -->
        <div class="col-xl-3 col-md-6">
            <div class="card bg-light border-5 border-primary text-dark mb-4">

                <div class="card-body"><strong>Frequência Respiratória</strong> 
                    <div class="card-footer d-flex align-items-center justify-content-between">
                    </div>
                    <h6 class="mb-0"> Máximo: <?php echo $value5Alto;?> ipm</h6>
                    <h6 class="mb-0"> Mínimo: <?php echo $value5Baixo;?> ipm</h6>
                    
                </div>
                
            </div>
        </div>

        <!--
        <div class="col-xl-3 col-md-6">
            <div class="card bg-primary text-white mb-4">
                <div class="card-body">Frequência Respiratória</div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <h6 class="mb-0"> Máximo : 29 </h6>
                    <h6 class="mb-0"> Mínimo: 18 </h6>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>
        --> 
    </div>
    
</div>
<!-- Mostrar grafico geral atravez do highcharts -->
<iframe src="../esp32_mysql_graficos/grafico/grafico_geral.php" id="myFrame" width="1120" height="480" allowfullscreen frameBorder="0" scrolling="no"></iframe>
<!-- fim de Mostrar grafico geral atravez do highcharts -->

<!--Mostrar grafico temperatura atravez do highcharts -->
<iframe src="../esp32_mysql_graficos/grafico/grafico_temperatura.php" id="myFrame" width="1120" height="480" allowfullscreen frameBorder="0" scrolling="no"></iframe>
<!-- fim de Mostrar grafico temperatura atravez do highcharts -->

<!-- Mostrar grafico frequencia cardiaca atravez do highcharts -->
<iframe src="../esp32_mysql_graficos/grafico/grafico_BPM.php" id="myFrame" width="1120" height="480" allowfullscreen frameBorder="0" scrolling="no"></iframe>
<!-- fim de Mostrar grafico frequencia atravez do highcharts -->


<!-- Mostrar grafico pressão artirial distolica atravez do highcharts--> 
<iframe src="../esp32_mysql_graficos/grafico/grafico_PAS.php" id="myFrame" width="1120" height="480" allowfullscreen frameBorder="0" scrolling="no"></iframe>
<!--fim de  Mostrar grafico pressão artiria diastolica atravez do highcharts -->

<!-- Mostrar grafico Frquencia respiratoria atravez do highcharts -->
<iframe src="../esp32_mysql_graficos/grafico/grafico_FR.php" id="myFrame" width="1120" height="480" allowfullscreen frameBorder="0" scrolling="no"></iframe>
<!-- fim de Mostrar grafico Frequencia respiratoria atravez do highcharts -->
 
<?php
include('includes/fooder.php');
include('includes/script.php');
?>