<?php
include('autenticacao.php');
include('includes/header.php');
?>

<div class="container-fluid px-4">
    <h4 class="mt-4">Utilizadores</h4>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Gestão</li>
        <li class="breadcrumb-item active">Utilizadores</li>
    </ol>
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>Editar Utilizador</h4>
                </div>
                <div class="card-body">

                    <?php
                    if (isset($_GET['id'])) {
                        $user_id = $_GET['id'];
                        $users = "SELECT * FROM users WHERE id = '$user_id' ";
                        $users_run = mysqli_query($conn, $users);

                        if (mysqli_num_rows($users_run) > 0) {
                            foreach ($users_run as $user)
                             {
                    ?>
                                <form action="codigo.php" method="POST">
                                    <input type="hidden" name="user_id" value="<?=$user['id'];?>">
                                    <div class="row">
                                        <div class="col-md-6 mb-3">
                                            <label for="">Nome</label>
                                            <input type="text" name="nome" value="<?= $user['nome']; ?>" class="form-control">
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Apedilio</label>
                                            <input type="text" name="Apedilio" value="<?= $user['Apedilio']; ?>" class="form-control">
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Email</label>
                                            <input type="text" name="email" value="<?= $user['email']; ?>" class="form-control">
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Senha</label>
                                            <input type="password" name="senha"  value="<?= $user['senha']; ?>" class="form-control">
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Telemóvel</label>
                                            <input type="number" name="telemovel"  value="<?= $user['telemovel']; ?>" class="form-control">
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Foto</label><br>
                                            <input type="hidden" name="old_foto2" value="<?= $user['foto_utilizador'] ?>">
                                                <input type="file" name="foto_utilizador" class="form-control" required>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Nível de acesso</label>
                                            <select name="role_as" required class="form-control">
                                                <option value="">--Selecionar Nível de acesso</option>
                                                <option value="1" <?= $user['role_as'] == '1' ? 'Selected' : '' ?>>Admin</option>
                                                <option value="0" <?= $user['role_as'] == '0' ? 'Selected' : '' ?>>Utilizador</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Estado</label>
                                            <input type="checkbox" name="status" <?= $user['status'] == '1' ? 'checked' : '' ?> width="70px" height="70px" />
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <button type="submit" name="atualizar-usuarios" class="btn btn-primary">Atualizar</button>
                                        </div>

                                    </div>
                                </form>
                            <?php
                            }
                        } else {
                            ?>
                            <h4>Nenhuma gravação encontrada</h4>
                    <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>

<?php
include('includes/fooder.php');
include('includes/script.php');
?>