<?php
include('autenticacao.php');
include('includes/header.php');


?>
<style>
    .border_utilizador{
        border-radius: 70px 70px 70px 70px;
    }
</style>

<div class="container-fluid px-4">
   <!-- <h4 class="mt-0">Utilizadores</h4>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Dashboard</li>
        <li class="breadcrumb-item active">Utilizadores</li>
    </ol>-->
    <div class="row mt-4">

        <div class="col-md-12">

            <?php include('menssagem.php'); ?>

            <div class="card">
           
                <div style="background:#bdc0c2;" class="card-header">
            
                    <h4>Utilizadores Registrados
            
                        <!--baixar lista de utilizadores -->
                        <button href="" class="btn btn-info float-end" id="baixar" style="margin: 0 15px;" ><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-download" viewBox="0 0 16 16">
                        <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                        <path d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"/>
                        </svg></button>

                        <!--Adicionar utilizadores-->
                        <a href="registrar-add.php" class="btn btn-primary float-end"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                        </svg></a>

                    </h4>   

                </div>
                <div class="card-body" id="utilizadores">

                  <div class="table-responsive">

                        <table id="tabela" class="table table table-bordered table-stripe border-3">
                            <thead style="background:#bdc0c2;">
                                <tr>
                                    <th>ID </th>
                                    <th>Nome</th>
                                    <th>Apedilio</th>
                                    <th>Email</th>
                                    <th>Telemóvel</th>
                                    <th>Foto</th>
                                    <th>Função</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $query = "SELECT *FROM users";
                                $query_run = mysqli_query($conn, $query);

                                if (mysqli_num_rows($query_run) > 0) {
                                    foreach ($query_run as $row) {
                                ?>
                                        <tr>
                                            <td class="user_id"><?= $row['id']; ?></td>
                                            <td><?= $row['nome']; ?></td>
                                            <td><?= $row['Apedilio']; ?></td>
                                            <td><?= $row['email']; ?></td>
                                            <td><?= $row['telemovel']; ?></td>
                                            <td><img src="../uploads2/fotos2/<?= $row['foto_utilizador'] ?>" width="100px" height="100px" class="border_utilizador" alt=""/></td>
                                            <td>
                                                <?php
                                                if ($row['role_as']  == '1') {
                                                    echo 'Admin';
                                                } elseif ($row['role_as']  == '0') {
                                                    echo 'Utilizador';
                                                }
                                                ?>
                                            </td>
                                            <td> <a href="registrar-editar.php?id=<?= $row['id']; ?> " class="btn btn-warning"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                                        </svg></a> </td>
                                            <td>
                                                <form action="codigo.php?id=<?= $row['id']; ?>" method="POST">
                                                    <button type="submit" name="eliminar-usuario"  value="<?=$row['id'];?>" class="eliminar-usuario btn btn-danger "><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                                    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                                    </svg></button>
                                                </form> 
                                            </td>
                                        
                                        </tr>
                                    <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="6">Nenhuma gravação encontrada</td>
                                    </tr>
                                <?php
                                }
                                ?>
                                
                                <!--<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"></script>-->
                                <!--<script src="js/personalizado.js"></script>-->
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                                <script src="assets/css/bootstrap/js/bootstrap.min.js"></script>
                                <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                                <script src="sweetalert2.min.js"></script>
                                <link rel="stylesheet" href="css/sweetalert2.min.css">  
                                <script src="assets/swal2/sweetalert2.min.js"></script>

                                <!----------------------------baixar em formato pdf utilizadores registrados------------------------------->
                                <script src="js/html2pdf.bundle.min.js"></script>
                                <script type="text/javascript" >

                                   document.getElementById('baixar').onclick = function(){
                                    var element = document.getElementById('tabela');

                                    var opt= {
                                        margin:       0.9,
                                        filename:     'Lista de utilizadores.pdf',
                                        image:        { type: 'jpeg', quality: 1},
                                        html2canvas:  { scale: 2 },
                                        jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait'}
                                    };
                                     html2pdf(element, opt);
                                   };

                                </script>
                                <!--fim baixar em formato pdf utilizadores registrados-->


                                <!--confirmar se deseja relmente eliminar utilizadores registrados-->
                                <script>
                                        $(document).ready(function(){
                                            
                                            
                                            $('.eliminar-usuario').click(function(e){
                                                
                                                e.preventDefault();
                                                var id = $(this).val();

                                                Swal.fire({
                                                    title: 'Tens certeza que desejas eliminar o utilizador?',
                                                    text: "Você não poderá reverter isso!",
                                                    icon: 'warning',
                                                    showCancelButton: true,
                                                    confirmButtonColor: '#3085d6',
                                                    cancelButtonColor: '#d33',
                                                    confirmButtonText: 'Sim, Eliminar!'
                                                }).then((result) => {

                                                            //window.location.reload();
                                                    
                                                if (result.isConfirmed) 
                                                {
                                                    window.location.reload();
                                                    $.ajax({
                                                        method: "POST",
                                                        url: "codigo.php",
                                                        data:{
                                                        'user_id':id,
                                                            'eliminar-usuario':true,
                                                        },
                                                        success: function(response){
                                                            if(response == 200)
                                                            {
                                                            
                                                                Swal.fire('success!','Utilizador eliminado com sucesso!','success');
                                                                $("#utilizadores").load(location.href + " #utilizadores");
                                                            }
                                                            else if(response == 500)
                                                            {
                                                                swal.fire("Erro!", "Algo correu mal!", "Error");
                                                            }
                                                        }
                                                    });
                                                }
                                                else
                                                {
                                                    swal.fire("O ficheiro está salvo!");
                                                }
                                                });
                                            });
                                            
                                        });
                                        
                                                                    
                                    </script>
                                     <!-- fim confirmar se deseja relmente eliminar utilizadores registrados-->
                            </tbody>
                        </table>
                  </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php
include('includes/fooder.php');
include('includes/script.php');
?>