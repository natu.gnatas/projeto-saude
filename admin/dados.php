<?php
include('autenticacao.php');
include('includes/header.php');

?>

<div class="container-fluid px-4">
    
    <div class="row mt-4">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4>Dados do Paciente
                       <!-------------------------Baixar Notas dos pacientes----------------------------------------->
                          
                       <button  href="" class="btn btn-info float-end" id="baixar" style="margin: 0 15px;"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-download" viewBox="0 0 16 16">
                        <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                        <path d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"/>
                        </svg></button>
                    </h4>
                </div>
                <div class="card-body">

                     <div class="table-responsive">
                            <table id="tabela" class="table table table-bordered table-stripe" >
                              <thead style="background:#bdc0c2;">
                                  <tr>
                                      <th>ID</th>
                                      <th>Sensor</th>
                                      <th>Localização</th>
                                      <th>Temperatura</th>
                                      <th>BPM</th>
                                      <th>PAS</th>
                                      <th>PAD</th>
                                      <th>Freq Respiratória</th>
                                      <th>Data e Hora</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <?php 

                               
                                    $dados = "SELECT *FROM sensordata";
                                    $dados_run = mysqli_query($conn, $dados);

                                    if(mysqli_num_rows($dados_run) > 0)
                                    {
                                        foreach($dados_run as $item)
                                        {
                                            ?>
                                            <tr>
                                                <td><?= $item['id'] ?></td>
                                                <td><?= $item['sensor'] ?></td>
                                                <td><?= $item['location'] ?></td>
                                                <td><?= $item['value1'] ?></td>
                                                <td><?= $item['value2'] ?></td>
                                                <td><?= $item['value3'] ?></td>
                                                <td><?= $item['value4'] ?></td>
                                                <td><?= $item['value5'] ?></td>
                                                <td><?= $item['reading_time'] ?></td>
                                               
                                            </tr>
                                            <?php
                                        }
                                    }
                                    else
                                    {
                                        ?>
                                            <tr>
                                                <td colspan="5">Nenhuma gravação encontrada</td>
                                            </tr>
                                        <?php
                                    }
                               ?>

                             <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                             <script src="assets/css/bootstrap/js/bootstrap.min.js"></script>
                             <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                             <script src="sweetalert2.min.js"></script>
                             <link rel="stylesheet" href="css/sweetalert2.min.css">  
                             <script src="assets/swal2/sweetalert2.min.js"></script>

                                 <!----------------------------baixar em formato pdf dados recolhidos------------------------------->
                                <script src="js/html2pdf.bundle.min.js"></script>
                                <script type="text/javascript" >

                                   document.getElementById('baixar').onclick = function(){
                                    var element = document.getElementById('tabela');

                                    var opt= {
                                        margin:       0,
                                        filename:     'Dados Recolhidos.pdf',
                                        image:        { type: 'jpeg', quality: 0.98},
                                        html2canvas:  { scale: 2 },
                                        jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait'}
                                    };
                                     html2pdf(element, opt);
                                   };

                                </script>
                                <!-------------------------------fim baixar em formato pdf dados recolhidos------------------------------>



                              </tbody>
                         </table>
                     </div>
                
                </div>
            </div>
        </div>

    </div>
</div>

<?php
include('includes/fooder.php');
include('includes/script.php');
?>