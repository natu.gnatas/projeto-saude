<?php
 include_once "conec_alerta.php";
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/9ea8094987.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="../alerta/css/estilo.css">

    <title>Patient Monitoring!</title>
  </head>
  <body>
    
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container" >
  <!--<a class="navbar-brand" href="#">Navbar</a>-->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <?php
    //$sql = mysqli_query($conn, "SELECT *FROM alerta WHERE view=0");
    //$count = mysqli_num_rows($sql);
  ?>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-envelope"></i> <span class="badge badge-danger" id="count"><?php echo mysqli_num_rows(mysqli_query($conn, "SELECT *FROM alerta WHERE view=0")); ?></span>
        </a>
        <div class="dropdown-list dropdown-menu dropdown-right shadow animated--grow-in" aria-labelledby="navbarDropdown ">
        <h6 class="dropdown-header text-danger"><i class="fas fa-exclamation-triangle"></i> Alertas de perigo</h6>

        <?php 
             $sql = mysqli_query($conn, "SELECT *FROM alerta ORDER BY data DESC LIMIT 4"); 
             if(mysqli_num_rows($sql)>0)
             {
               while($res = mysqli_fetch_assoc($sql))
               {
                  echo '<a class="dropdown-item d-flex align-items-center" href="#">
                         <div class="mr-3 ">
                            <div class="icon-circle bg-warning">
                            <i class="fas fa-exclamation-triangle text-white"></i>
                            </div>
                         </div>
                    <div>
                      <div class="small text-gray-500">'.$res['data'].'</div>
                    '.$res['menssagem'].'
                    </div>
                    </a>';

                
                /*echo ' <a class="dropdown-item text-danger font-weight-bold" href="#">'.$res['menssagem'].'</a>';
                echo '<div class="dropdown-divider"></div>';*/
               }
             }
             else
             {
                echo ' <a class="dropdown-item text-primary font-weight-bold" href="#"> <i class="fas fa-thumbs-up"></i> Nenhuma Alerta</a>';
             }
           ?>
           <?php echo '<a class="dropdown-item text-center small text-gray-500" href="alerta.php">Ver todas alertas</a>';?>
           
              
      </li>
    </ul>
  </div>
  </div>
</nav>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   
  </body>
</html>