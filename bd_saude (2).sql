-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2022 at 02:53 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bd_saude`
--

-- --------------------------------------------------------

--
-- Table structure for table `alerta`
--

CREATE TABLE `alerta` (
  `id` int(11) NOT NULL,
  `menssagem` text NOT NULL,
  `data` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `view` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alerta`
--

INSERT INTO `alerta` (`id`, `menssagem`, `data`, `view`) VALUES
(82, 'Temperatura problema: T (258.00 V) fora do parametro', '2022-01-10 13:44:50', 1),
(83, 'Frequência cardiéca Problema: FC (258.00 V) fora do parametro', '2022-01-21 17:53:55', 1),
(84, 'Pressão Artirial: PA (258.00 V) fora do parametro', '2022-01-21 17:53:55', 1),
(0, 'Paulo:Temperatura (28.50 ºC) fora da normalidade', '2022-06-03 21:59:09', 0),
(0, 'Paulo:Frequência cardiéca (55 bpm) fora da normalidade', '2022-06-03 21:59:09', 0),
(0, 'Paulo: Frequência Respiratória (28.50 ipm) fora da normalidade', '2022-06-03 21:59:09', 0),
(0, 'Paulo:Temperatura (28.50 ºC) fora da normalidade', '2022-06-03 21:59:32', 0),
(0, 'Paulo:Frequência cardiéca (174 bpm) fora da normalidade', '2022-06-03 21:59:32', 0),
(0, 'Paulo: Frequência Respiratória (28.50 ipm) fora da normalidade', '2022-06-03 21:59:32', 0),
(0, 'Paulo:Temperatura (28.50 ºC) fora da normalidade', '2022-06-03 21:59:55', 0),
(0, 'Paulo:Frequência cardiéca (147 bpm) fora da normalidade', '2022-06-03 21:59:55', 0),
(0, 'Paulo:Pressão Artirial (153/103 mmHg) fora da normalidade', '2022-06-03 21:59:55', 0),
(0, 'Paulo: Frequência Respiratória (28.50 ipm) fora da normalidade', '2022-06-03 21:59:55', 0),
(0, 'Paulo:Temperatura (28.50 ºC) fora da normalidade', '2022-06-03 22:00:18', 0),
(0, 'Paulo:Frequência cardiéca (136 bpm) fora da normalidade', '2022-06-03 22:00:18', 0),
(0, 'Paulo:Pressão Artirial (127/82 mmHg) fora da normalidade', '2022-06-03 22:00:18', 0),
(0, 'Paulo: Frequência Respiratória (28.50 ipm) fora da normalidade', '2022-06-03 22:00:18', 0),
(0, 'Paulo:Temperatura (28.50 ºC) fora da normalidade', '2022-06-03 22:00:42', 0),
(0, 'Paulo:Frequência cardiéca (115 bpm) fora da normalidade', '2022-06-03 22:00:42', 0),
(0, 'Paulo:Pressão Artirial (169/71 mmHg) fora da normalidade', '2022-06-03 22:00:42', 0),
(0, 'Paulo: Frequência Respiratória (28.50 ipm) fora da normalidade', '2022-06-03 22:00:42', 0),
(0, 'Paulo:Temperatura (28.50 ºC) fora da normalidade', '2022-06-03 22:01:05', 0),
(0, 'Paulo:Pressão Artirial (135/74 mmHg) fora da normalidade', '2022-06-03 22:01:05', 0),
(0, 'Paulo: Frequência Respiratória (28.50 ipm) fora da normalidade', '2022-06-03 22:01:05', 0),
(0, 'Paulo:Temperatura (28.50 ºC) fora da normalidade', '2022-06-03 22:01:28', 0),
(0, 'Paulo:Frequência cardiéca (168 bpm) fora da normalidade', '2022-06-03 22:01:28', 0),
(0, 'Paulo:Pressão Artirial (162/77 mmHg) fora da normalidade', '2022-06-03 22:01:28', 0),
(0, 'Paulo: Frequência Respiratória (28.50 ipm) fora da normalidade', '2022-06-03 22:01:28', 0),
(0, 'Paulo:Temperatura (28.50 ºC) fora da normalidade', '2022-06-03 22:01:51', 0),
(0, 'Paulo: Frequência Respiratória (28.50 ipm) fora da normalidade', '2022-06-03 22:01:51', 0),
(0, 'Paulo:Temperatura (28.50 ºC) fora da normalidade', '2022-06-03 22:02:15', 0),
(0, 'Paulo:Pressão Artirial (178/98 mmHg) fora da normalidade', '2022-06-03 22:02:15', 0),
(0, 'Paulo: Frequência Respiratória (28.50 ipm) fora da normalidade', '2022-06-03 22:02:15', 0),
(0, 'Paulo:Temperatura (28.56 ºC) fora da normalidade', '2022-06-03 22:02:38', 0),
(0, 'Paulo:Frequência cardiéca (143 bpm) fora da normalidade', '2022-06-03 22:02:38', 0),
(0, 'Paulo:Pressão Artirial (127/87 mmHg) fora da normalidade', '2022-06-03 22:02:38', 0),
(0, 'Paulo: Frequência Respiratória (28.56 ipm) fora da normalidade', '2022-06-03 22:02:38', 0),
(0, 'Paulo:Temperatura (28.62 ºC) fora da normalidade', '2022-06-03 22:03:01', 0),
(0, 'Paulo: Frequência Respiratória (28.62 ipm) fora da normalidade', '2022-06-03 22:03:01', 0),
(0, 'Paulo:Temperatura (28.62 ºC) fora da normalidade', '2022-06-03 22:03:24', 0),
(0, 'Paulo:Pressão Artirial (153/78 mmHg) fora da normalidade', '2022-06-03 22:03:24', 0),
(0, 'Paulo: Frequência Respiratória (28.62 ipm) fora da normalidade', '2022-06-03 22:03:24', 0),
(0, 'Paulo:Temperatura (28.56 ºC) fora da normalidade', '2022-06-03 22:03:47', 0),
(0, 'Paulo:Frequência cardiéca (59 bpm) fora da normalidade', '2022-06-03 22:03:47', 0),
(0, 'Paulo:Pressão Artirial (181/85 mmHg) fora da normalidade', '2022-06-03 22:03:47', 0),
(0, 'Paulo: Frequência Respiratória (28.56 ipm) fora da normalidade', '2022-06-03 22:03:47', 0),
(0, 'Paulo:Temperatura (28.56 ºC) fora da normalidade', '2022-06-03 22:04:10', 0),
(0, 'Paulo:Pressão Artirial (151/76 mmHg) fora da normalidade', '2022-06-03 22:04:10', 0),
(0, 'Paulo: Frequência Respiratória (28.56 ipm) fora da normalidade', '2022-06-03 22:04:10', 0),
(0, 'Paulo:Temperatura (28.50 ºC) fora da normalidade', '2022-06-03 22:04:33', 0),
(0, 'Paulo:Pressão Artirial (178/78 mmHg) fora da normalidade', '2022-06-03 22:04:33', 0),
(0, 'Paulo: Frequência Respiratória (28.50 ipm) fora da normalidade', '2022-06-03 22:04:33', 0),
(0, 'Paulo:Temperatura (28.50 ºC) fora da normalidade', '2022-06-03 22:04:56', 0),
(0, 'Paulo:Frequência cardiéca (190 bpm) fora da normalidade', '2022-06-03 22:04:56', 0),
(0, 'Paulo:Pressão Artirial (140/74 mmHg) fora da normalidade', '2022-06-03 22:04:56', 0),
(0, 'Paulo: Frequência Respiratória (28.50 ipm) fora da normalidade', '2022-06-03 22:04:56', 0),
(0, 'Paulo:Temperatura (28.50 ºC) fora da normalidade', '2022-06-03 22:05:19', 0),
(0, 'Paulo:Frequência cardiéca (145 bpm) fora da normalidade', '2022-06-03 22:05:19', 0),
(0, 'Paulo:Pressão Artirial (152/102 mmHg) fora da normalidade', '2022-06-03 22:05:19', 0),
(0, 'Paulo: Frequência Respiratória (28.50 ipm) fora da normalidade', '2022-06-03 22:05:19', 0),
(0, 'Paulo:Temperatura (28.44 ºC) fora da normalidade', '2022-06-03 22:05:42', 0),
(0, 'Paulo:Frequência cardiéca (58 bpm) fora da normalidade', '2022-06-03 22:05:42', 0),
(0, 'Paulo:Pressão Artirial (167/99 mmHg) fora da normalidade', '2022-06-03 22:05:42', 0),
(0, 'Paulo: Frequência Respiratória (28.44 ipm) fora da normalidade', '2022-06-03 22:05:42', 0),
(0, 'Paulo:Temperatura (28.38 ºC) fora da normalidade', '2022-06-03 22:06:05', 0),
(0, 'Paulo:Frequência cardiéca (196 bpm) fora da normalidade', '2022-06-03 22:06:05', 0),
(0, 'Paulo: Frequência Respiratória (28.38 ipm) fora da normalidade', '2022-06-03 22:06:05', 0),
(0, 'Paulo:Temperatura (28.38 ºC) fora da normalidade', '2022-06-03 22:06:29', 0),
(0, 'Paulo:Frequência cardiéca (184 bpm) fora da normalidade', '2022-06-03 22:06:29', 0),
(0, 'Paulo:Pressão Artirial (186/95 mmHg) fora da normalidade', '2022-06-03 22:06:29', 0),
(0, 'Paulo: Frequência Respiratória (28.38 ipm) fora da normalidade', '2022-06-03 22:06:29', 0),
(0, 'Paulo:Temperatura (27.87 ºC) fora da normalidade', '2022-06-03 22:07:37', 0),
(0, 'Paulo:Frequência cardiéca (110 bpm) fora da normalidade', '2022-06-03 22:07:37', 0),
(0, 'Paulo: Frequência Respiratória (27.87 ipm) fora da normalidade', '2022-06-03 22:07:37', 0),
(0, 'Paulo:Temperatura (27.81 ºC) fora da normalidade', '2022-06-03 22:08:01', 0),
(0, 'Paulo:Frequência cardiéca (195 bpm) fora da normalidade', '2022-06-03 22:08:01', 0),
(0, 'Paulo: Frequência Respiratória (27.81 ipm) fora da normalidade', '2022-06-03 22:08:01', 0),
(0, 'Paulo:Temperatura (27.69 ºC) fora da normalidade', '2022-06-03 22:08:24', 0),
(0, 'Paulo:Pressão Artirial (164/96 mmHg) fora da normalidade', '2022-06-03 22:08:24', 0),
(0, 'Paulo: Frequência Respiratória (27.69 ipm) fora da normalidade', '2022-06-03 22:08:24', 0),
(0, 'Paulo:Temperatura (27.62 ºC) fora da normalidade', '2022-06-03 22:08:47', 0),
(0, 'Paulo:Frequência cardiéca (102 bpm) fora da normalidade', '2022-06-03 22:08:47', 0),
(0, 'Paulo:Pressão Artirial (144/83 mmHg) fora da normalidade', '2022-06-03 22:08:47', 0),
(0, 'Paulo: Frequência Respiratória (27.62 ipm) fora da normalidade', '2022-06-03 22:08:47', 0),
(0, 'Paulo:Temperatura (27.56 ºC) fora da normalidade', '2022-06-03 22:09:10', 0),
(0, 'Paulo:Pressão Artirial (182/93 mmHg) fora da normalidade', '2022-06-03 22:09:10', 0),
(0, 'Paulo: Frequência Respiratória (27.56 ipm) fora da normalidade', '2022-06-03 22:09:10', 0),
(0, 'Paulo:Temperatura (27.50 ºC) fora da normalidade', '2022-06-03 22:09:33', 0),
(0, 'Paulo:Frequência cardiéca (189 bpm) fora da normalidade', '2022-06-03 22:09:33', 0),
(0, 'Paulo:Pressão Artirial (152/74 mmHg) fora da normalidade', '2022-06-03 22:09:33', 0),
(0, 'Paulo: Frequência Respiratória (27.50 ipm) fora da normalidade', '2022-06-03 22:09:33', 0),
(0, 'Paulo:Temperatura (27.50 ºC) fora da normalidade', '2022-06-03 22:09:56', 0),
(0, 'Paulo:Frequência cardiéca (139 bpm) fora da normalidade', '2022-06-03 22:09:56', 0),
(0, 'Paulo:Pressão Artirial (186/77 mmHg) fora da normalidade', '2022-06-03 22:09:56', 0),
(0, 'Paulo: Frequência Respiratória (27.50 ipm) fora da normalidade', '2022-06-03 22:09:56', 0),
(0, 'Paulo:Temperatura (27.44 ºC) fora da normalidade', '2022-06-03 22:10:19', 0),
(0, 'Paulo:Pressão Artirial (170/77 mmHg) fora da normalidade', '2022-06-03 22:10:19', 0),
(0, 'Paulo: Frequência Respiratória (27.44 ipm) fora da normalidade', '2022-06-03 22:10:19', 0),
(0, 'Paulo:Temperatura (27.37 ºC) fora da normalidade', '2022-06-03 22:10:42', 0),
(0, 'Paulo:Frequência cardiéca (184 bpm) fora da normalidade', '2022-06-03 22:10:42', 0),
(0, 'Paulo: Frequência Respiratória (27.37 ipm) fora da normalidade', '2022-06-03 22:10:42', 0),
(0, 'Paulo:Temperatura (27.31 ºC) fora da normalidade', '2022-06-03 22:11:05', 0),
(0, 'Paulo:Frequência cardiéca (147 bpm) fora da normalidade', '2022-06-03 22:11:05', 0),
(0, 'Paulo:Pressão Artirial (156/78 mmHg) fora da normalidade', '2022-06-03 22:11:05', 0),
(0, 'Paulo: Frequência Respiratória (27.31 ipm) fora da normalidade', '2022-06-03 22:11:05', 0),
(0, 'Paulo:Temperatura (27.31 ºC) fora da normalidade', '2022-06-03 22:11:29', 0),
(0, 'Paulo:Frequência cardiéca (125 bpm) fora da normalidade', '2022-06-03 22:11:29', 0),
(0, 'Paulo:Pressão Artirial (175/101 mmHg) fora da normalidade', '2022-06-03 22:11:29', 0),
(0, 'Paulo: Frequência Respiratória (27.31 ipm) fora da normalidade', '2022-06-03 22:11:29', 0),
(0, 'Paulo:Temperatura (27.31 ºC) fora da normalidade', '2022-06-03 22:11:52', 0),
(0, 'Paulo:Frequência cardiéca (154 bpm) fora da normalidade', '2022-06-03 22:11:52', 0),
(0, 'Paulo:Pressão Artirial (123/96 mmHg) fora da normalidade', '2022-06-03 22:11:52', 0),
(0, 'Paulo: Frequência Respiratória (27.31 ipm) fora da normalidade', '2022-06-03 22:11:52', 0),
(0, 'Paulo:Temperatura (27.25 ºC) fora da normalidade', '2022-06-03 22:12:15', 0),
(0, 'Paulo:Frequência cardiéca (143 bpm) fora da normalidade', '2022-06-03 22:12:15', 0),
(0, 'Paulo:Pressão Artirial (154/84 mmHg) fora da normalidade', '2022-06-03 22:12:15', 0),
(0, 'Paulo: Frequência Respiratória (27.25 ipm) fora da normalidade', '2022-06-03 22:12:15', 0),
(0, 'Paulo:Temperatura (27.25 ºC) fora da normalidade', '2022-06-03 22:12:38', 0),
(0, 'Paulo:Frequência cardiéca (165 bpm) fora da normalidade', '2022-06-03 22:12:38', 0),
(0, 'Paulo:Pressão Artirial (166/82 mmHg) fora da normalidade', '2022-06-03 22:12:38', 0),
(0, 'Paulo: Frequência Respiratória (27.25 ipm) fora da normalidade', '2022-06-03 22:12:38', 0),
(0, 'Paulo:Temperatura (27.25 ºC) fora da normalidade', '2022-06-03 22:13:01', 0),
(0, 'Paulo:Pressão Artirial (132/81 mmHg) fora da normalidade', '2022-06-03 22:13:01', 0),
(0, 'Paulo: Frequência Respiratória (27.25 ipm) fora da normalidade', '2022-06-03 22:13:01', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:13:24', 0),
(0, 'Paulo:Frequência cardiéca (141 bpm) fora da normalidade', '2022-06-03 22:13:24', 0),
(0, 'Paulo:Pressão Artirial (124/97 mmHg) fora da normalidade', '2022-06-03 22:13:24', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:13:24', 0),
(0, 'Paulo:Temperatura (27.25 ºC) fora da normalidade', '2022-06-03 22:13:47', 0),
(0, 'Paulo:Frequência cardiéca (128 bpm) fora da normalidade', '2022-06-03 22:13:47', 0),
(0, 'Paulo:Pressão Artirial (167/100 mmHg) fora da normalidade', '2022-06-03 22:13:47', 0),
(0, 'Paulo: Frequência Respiratória (27.25 ipm) fora da normalidade', '2022-06-03 22:13:47', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:14:10', 0),
(0, 'Paulo:Frequência cardiéca (56 bpm) fora da normalidade', '2022-06-03 22:14:10', 0),
(0, 'Paulo:Pressão Artirial (184/104 mmHg) fora da normalidade', '2022-06-03 22:14:10', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:14:10', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:14:33', 0),
(0, 'Paulo:Frequência cardiéca (105 bpm) fora da normalidade', '2022-06-03 22:14:33', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:14:33', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:14:57', 0),
(0, 'Paulo:Frequência cardiéca (112 bpm) fora da normalidade', '2022-06-03 22:14:57', 0),
(0, 'Paulo:Pressão Artirial (151/102 mmHg) fora da normalidade', '2022-06-03 22:14:57', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:14:57', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:15:20', 0),
(0, 'Paulo:Frequência cardiéca (173 bpm) fora da normalidade', '2022-06-03 22:15:20', 0),
(0, 'Paulo:Pressão Artirial (128/81 mmHg) fora da normalidade', '2022-06-03 22:15:20', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:15:20', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:15:42', 0),
(0, 'Paulo:Frequência cardiéca (114 bpm) fora da normalidade', '2022-06-03 22:15:42', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:15:42', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:16:06', 0),
(0, 'Paulo:Frequência cardiéca (104 bpm) fora da normalidade', '2022-06-03 22:16:06', 0),
(0, 'Paulo:Pressão Artirial (186/73 mmHg) fora da normalidade', '2022-06-03 22:16:06', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:16:06', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:16:28', 0),
(0, 'Paulo:Frequência cardiéca (171 bpm) fora da normalidade', '2022-06-03 22:16:28', 0),
(0, 'Paulo:Pressão Artirial (145/80 mmHg) fora da normalidade', '2022-06-03 22:16:28', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:16:28', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:16:51', 0),
(0, 'Paulo:Pressão Artirial (163/70 mmHg) fora da normalidade', '2022-06-03 22:16:51', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:16:51', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:17:14', 0),
(0, 'Paulo:Pressão Artirial (130/100 mmHg) fora da normalidade', '2022-06-03 22:17:14', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:17:14', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:17:37', 0),
(0, 'Paulo:Frequência cardiéca (177 bpm) fora da normalidade', '2022-06-03 22:17:37', 0),
(0, 'Paulo:Pressão Artirial (145/105 mmHg) fora da normalidade', '2022-06-03 22:17:37', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:17:37', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:18:00', 0),
(0, 'Paulo:Frequência cardiéca (113 bpm) fora da normalidade', '2022-06-03 22:18:00', 0),
(0, 'Paulo:Pressão Artirial (156/70 mmHg) fora da normalidade', '2022-06-03 22:18:00', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:18:00', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:18:24', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:18:24', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:18:47', 0),
(0, 'Paulo:Frequência cardiéca (143 bpm) fora da normalidade', '2022-06-03 22:18:47', 0),
(0, 'Paulo:Pressão Artirial (148/84 mmHg) fora da normalidade', '2022-06-03 22:18:47', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:18:47', 0),
(0, 'Paulo:Temperatura (27.31 ºC) fora da normalidade', '2022-06-03 22:19:10', 0),
(0, 'Paulo:Frequência cardiéca (165 bpm) fora da normalidade', '2022-06-03 22:19:10', 0),
(0, 'Paulo:Pressão Artirial (135/91 mmHg) fora da normalidade', '2022-06-03 22:19:10', 0),
(0, 'Paulo: Frequência Respiratória (27.31 ipm) fora da normalidade', '2022-06-03 22:19:10', 0),
(0, 'Paulo:Temperatura (27.31 ºC) fora da normalidade', '2022-06-03 22:19:33', 0),
(0, 'Paulo:Frequência cardiéca (173 bpm) fora da normalidade', '2022-06-03 22:19:33', 0),
(0, 'Paulo:Pressão Artirial (170/96 mmHg) fora da normalidade', '2022-06-03 22:19:33', 0),
(0, 'Paulo: Frequência Respiratória (27.31 ipm) fora da normalidade', '2022-06-03 22:19:33', 0),
(0, 'Paulo:Temperatura (27.31 ºC) fora da normalidade', '2022-06-03 22:20:33', 0),
(0, 'Paulo:Pressão Artirial (152/77 mmHg) fora da normalidade', '2022-06-03 22:20:33', 0),
(0, 'Paulo: Frequência Respiratória (27.31 ipm) fora da normalidade', '2022-06-03 22:20:33', 0),
(0, 'Paulo:Temperatura (27.25 ºC) fora da normalidade', '2022-06-03 22:20:56', 0),
(0, 'Paulo:Frequência cardiéca (196 bpm) fora da normalidade', '2022-06-03 22:20:56', 0),
(0, 'Paulo:Pressão Artirial (125/83 mmHg) fora da normalidade', '2022-06-03 22:20:56', 0),
(0, 'Paulo: Frequência Respiratória (27.25 ipm) fora da normalidade', '2022-06-03 22:20:56', 0),
(0, 'Paulo:Temperatura (27.25 ºC) fora da normalidade', '2022-06-03 22:21:19', 0),
(0, 'Paulo:Frequência cardiéca (193 bpm) fora da normalidade', '2022-06-03 22:21:19', 0),
(0, 'Paulo:Pressão Artirial (175/96 mmHg) fora da normalidade', '2022-06-03 22:21:19', 0),
(0, 'Paulo: Frequência Respiratória (27.25 ipm) fora da normalidade', '2022-06-03 22:21:19', 0),
(0, 'Paulo:Temperatura (27.25 ºC) fora da normalidade', '2022-06-03 22:21:42', 0),
(0, 'Paulo:Frequência cardiéca (125 bpm) fora da normalidade', '2022-06-03 22:21:42', 0),
(0, 'Paulo: Frequência Respiratória (27.25 ipm) fora da normalidade', '2022-06-03 22:21:42', 0),
(0, 'Paulo:Temperatura (27.25 ºC) fora da normalidade', '2022-06-03 22:22:05', 0),
(0, 'Paulo:Frequência cardiéca (116 bpm) fora da normalidade', '2022-06-03 22:22:05', 0),
(0, 'Paulo: Frequência Respiratória (27.25 ipm) fora da normalidade', '2022-06-03 22:22:05', 0),
(0, 'Paulo:Temperatura (27.25 ºC) fora da normalidade', '2022-06-03 22:22:28', 0),
(0, 'Paulo:Frequência cardiéca (51 bpm) fora da normalidade', '2022-06-03 22:22:28', 0),
(0, 'Paulo:Pressão Artirial (133/70 mmHg) fora da normalidade', '2022-06-03 22:22:28', 0),
(0, 'Paulo: Frequência Respiratória (27.25 ipm) fora da normalidade', '2022-06-03 22:22:28', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:22:51', 0),
(0, 'Paulo:Pressão Artirial (176/100 mmHg) fora da normalidade', '2022-06-03 22:22:51', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:22:51', 0),
(0, 'Paulo:Temperatura (27.25 ºC) fora da normalidade', '2022-06-03 22:23:15', 0),
(0, 'Paulo:Frequência cardiéca (179 bpm) fora da normalidade', '2022-06-03 22:23:15', 0),
(0, 'Paulo:Pressão Artirial (129/108 mmHg) fora da normalidade', '2022-06-03 22:23:15', 0),
(0, 'Paulo: Frequência Respiratória (27.25 ipm) fora da normalidade', '2022-06-03 22:23:15', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:23:38', 0),
(0, 'Paulo:Frequência cardiéca (165 bpm) fora da normalidade', '2022-06-03 22:23:38', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:23:38', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:24:01', 0),
(0, 'Paulo:Frequência cardiéca (168 bpm) fora da normalidade', '2022-06-03 22:24:01', 0),
(0, 'Paulo:Pressão Artirial (135/75 mmHg) fora da normalidade', '2022-06-03 22:24:01', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:24:01', 0),
(0, 'Paulo:Temperatura (27.25 ºC) fora da normalidade', '2022-06-03 22:24:24', 0),
(0, 'Paulo:Frequência cardiéca (124 bpm) fora da normalidade', '2022-06-03 22:24:24', 0),
(0, 'Paulo:Pressão Artirial (168/98 mmHg) fora da normalidade', '2022-06-03 22:24:24', 0),
(0, 'Paulo: Frequência Respiratória (27.25 ipm) fora da normalidade', '2022-06-03 22:24:24', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:24:48', 0),
(0, 'Paulo:Frequência cardiéca (53 bpm) fora da normalidade', '2022-06-03 22:24:48', 0),
(0, 'Paulo:Pressão Artirial (142/79 mmHg) fora da normalidade', '2022-06-03 22:24:48', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:24:48', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:25:11', 0),
(0, 'Paulo:Frequência cardiéca (52 bpm) fora da normalidade', '2022-06-03 22:25:11', 0),
(0, 'Paulo:Pressão Artirial (134/75 mmHg) fora da normalidade', '2022-06-03 22:25:11', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:25:11', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:25:34', 0),
(0, 'Paulo:Frequência cardiéca (182 bpm) fora da normalidade', '2022-06-03 22:25:34', 0),
(0, 'Paulo:Pressão Artirial (174/101 mmHg) fora da normalidade', '2022-06-03 22:25:34', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:25:34', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:25:58', 0),
(0, 'Paulo:Frequência cardiéca (134 bpm) fora da normalidade', '2022-06-03 22:25:58', 0),
(0, 'Paulo:Pressão Artirial (136/79 mmHg) fora da normalidade', '2022-06-03 22:25:58', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:25:58', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:26:21', 0),
(0, 'Paulo:Pressão Artirial (188/74 mmHg) fora da normalidade', '2022-06-03 22:26:21', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:26:21', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:26:44', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:26:44', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:27:07', 0),
(0, 'Paulo:Frequência cardiéca (106 bpm) fora da normalidade', '2022-06-03 22:27:07', 0),
(0, 'Paulo:Pressão Artirial (173/77 mmHg) fora da normalidade', '2022-06-03 22:27:07', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:27:07', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:27:30', 0),
(0, 'Paulo:Frequência cardiéca (112 bpm) fora da normalidade', '2022-06-03 22:27:30', 0),
(0, 'Paulo:Pressão Artirial (131/84 mmHg) fora da normalidade', '2022-06-03 22:27:30', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:27:30', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:27:53', 0),
(0, 'Paulo:Frequência cardiéca (157 bpm) fora da normalidade', '2022-06-03 22:27:53', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:27:53', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:28:16', 0),
(0, 'Paulo:Frequência cardiéca (107 bpm) fora da normalidade', '2022-06-03 22:28:16', 0),
(0, 'Paulo:Pressão Artirial (169/72 mmHg) fora da normalidade', '2022-06-03 22:28:16', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:28:16', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:28:39', 0),
(0, 'Paulo:Frequência cardiéca (56 bpm) fora da normalidade', '2022-06-03 22:28:39', 0),
(0, 'Paulo:Pressão Artirial (137/77 mmHg) fora da normalidade', '2022-06-03 22:28:39', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:28:39', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:29:03', 0),
(0, 'Paulo:Frequência cardiéca (150 bpm) fora da normalidade', '2022-06-03 22:29:03', 0),
(0, 'Paulo:Pressão Artirial (138/101 mmHg) fora da normalidade', '2022-06-03 22:29:03', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:29:03', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:29:56', 0),
(0, 'Paulo:Frequência cardiéca (117 bpm) fora da normalidade', '2022-06-03 22:29:56', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:29:56', 0),
(0, 'Paulo:Temperatura (27.19 ºC) fora da normalidade', '2022-06-03 22:30:19', 0),
(0, 'Paulo:Frequência cardiéca (136 bpm) fora da normalidade', '2022-06-03 22:30:19', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 22:30:19', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:30:42', 0),
(0, 'Paulo:Frequência cardiéca (125 bpm) fora da normalidade', '2022-06-03 22:30:42', 0),
(0, 'Paulo:Pressão Artirial (137/75 mmHg) fora da normalidade', '2022-06-03 22:30:42', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:30:42', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:31:05', 0),
(0, 'Paulo:Frequência cardiéca (190 bpm) fora da normalidade', '2022-06-03 22:31:05', 0),
(0, 'Paulo:Pressão Artirial (161/91 mmHg) fora da normalidade', '2022-06-03 22:31:05', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:31:05', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:31:29', 0),
(0, 'Paulo:Frequência cardiéca (182 bpm) fora da normalidade', '2022-06-03 22:31:29', 0),
(0, 'Paulo:Pressão Artirial (150/72 mmHg) fora da normalidade', '2022-06-03 22:31:29', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:31:29', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:31:52', 0),
(0, 'Paulo:Frequência cardiéca (128 bpm) fora da normalidade', '2022-06-03 22:31:52', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:31:52', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:32:15', 0),
(0, 'Paulo:Pressão Artirial (132/93 mmHg) fora da normalidade', '2022-06-03 22:32:15', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:32:15', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:32:38', 0),
(0, 'Paulo:Frequência cardiéca (177 bpm) fora da normalidade', '2022-06-03 22:32:38', 0),
(0, 'Paulo:Pressão Artirial (158/70 mmHg) fora da normalidade', '2022-06-03 22:32:38', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:32:38', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:33:01', 0),
(0, 'Paulo:Frequência cardiéca (169 bpm) fora da normalidade', '2022-06-03 22:33:01', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:33:01', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:33:24', 0),
(0, 'Paulo:Frequência cardiéca (179 bpm) fora da normalidade', '2022-06-03 22:33:24', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:33:24', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:33:47', 0),
(0, 'Paulo:Frequência cardiéca (124 bpm) fora da normalidade', '2022-06-03 22:33:47', 0),
(0, 'Paulo:Pressão Artirial (173/107 mmHg) fora da normalidade', '2022-06-03 22:33:47', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:33:47', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:34:10', 0),
(0, 'Paulo:Frequência cardiéca (106 bpm) fora da normalidade', '2022-06-03 22:34:10', 0),
(0, 'Paulo:Pressão Artirial (169/92 mmHg) fora da normalidade', '2022-06-03 22:34:10', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:34:10', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:34:34', 0),
(0, 'Paulo:Frequência cardiéca (134 bpm) fora da normalidade', '2022-06-03 22:34:34', 0),
(0, 'Paulo:Pressão Artirial (159/74 mmHg) fora da normalidade', '2022-06-03 22:34:34', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:34:34', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:34:57', 0),
(0, 'Paulo:Frequência cardiéca (158 bpm) fora da normalidade', '2022-06-03 22:34:57', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:34:57', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:35:20', 0),
(0, 'Paulo:Frequência cardiéca (181 bpm) fora da normalidade', '2022-06-03 22:35:20', 0),
(0, 'Paulo:Pressão Artirial (128/83 mmHg) fora da normalidade', '2022-06-03 22:35:20', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:35:20', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:35:43', 0),
(0, 'Paulo:Frequência cardiéca (175 bpm) fora da normalidade', '2022-06-03 22:35:43', 0),
(0, 'Paulo:Pressão Artirial (146/75 mmHg) fora da normalidade', '2022-06-03 22:35:43', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:35:43', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:36:06', 0),
(0, 'Paulo:Frequência cardiéca (199 bpm) fora da normalidade', '2022-06-03 22:36:06', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:36:06', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:36:29', 0),
(0, 'Paulo:Frequência cardiéca (184 bpm) fora da normalidade', '2022-06-03 22:36:29', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:36:29', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:36:52', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:36:52', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:37:15', 0),
(0, 'Paulo:Pressão Artirial (189/98 mmHg) fora da normalidade', '2022-06-03 22:37:15', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:37:15', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:37:38', 0),
(0, 'Paulo:Frequência cardiéca (116 bpm) fora da normalidade', '2022-06-03 22:37:38', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:37:38', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:38:02', 0),
(0, 'Paulo:Pressão Artirial (181/77 mmHg) fora da normalidade', '2022-06-03 22:38:02', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:38:02', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:38:24', 0),
(0, 'Paulo:Frequência cardiéca (140 bpm) fora da normalidade', '2022-06-03 22:38:24', 0),
(0, 'Paulo:Pressão Artirial (186/85 mmHg) fora da normalidade', '2022-06-03 22:38:24', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:38:24', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:38:48', 0),
(0, 'Paulo:Frequência cardiéca (152 bpm) fora da normalidade', '2022-06-03 22:38:48', 0),
(0, 'Paulo:Pressão Artirial (121/87 mmHg) fora da normalidade', '2022-06-03 22:38:48', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:38:48', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:39:11', 0),
(0, 'Paulo:Frequência cardiéca (128 bpm) fora da normalidade', '2022-06-03 22:39:11', 0),
(0, 'Paulo:Pressão Artirial (159/76 mmHg) fora da normalidade', '2022-06-03 22:39:11', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:39:11', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:39:34', 0),
(0, 'Paulo:Frequência cardiéca (104 bpm) fora da normalidade', '2022-06-03 22:39:34', 0),
(0, 'Paulo:Pressão Artirial (176/97 mmHg) fora da normalidade', '2022-06-03 22:39:34', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:39:34', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:39:57', 0),
(0, 'Paulo:Frequência cardiéca (124 bpm) fora da normalidade', '2022-06-03 22:39:57', 0),
(0, 'Paulo:Pressão Artirial (153/90 mmHg) fora da normalidade', '2022-06-03 22:39:57', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:39:57', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:40:20', 0),
(0, 'Paulo:Frequência cardiéca (141 bpm) fora da normalidade', '2022-06-03 22:40:20', 0),
(0, 'Paulo:Pressão Artirial (180/96 mmHg) fora da normalidade', '2022-06-03 22:40:20', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:40:20', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:40:43', 0),
(0, 'Paulo:Frequência cardiéca (125 bpm) fora da normalidade', '2022-06-03 22:40:43', 0),
(0, 'Paulo:Pressão Artirial (129/93 mmHg) fora da normalidade', '2022-06-03 22:40:43', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:40:43', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:41:06', 0),
(0, 'Paulo:Frequência cardiéca (176 bpm) fora da normalidade', '2022-06-03 22:41:06', 0),
(0, 'Paulo:Pressão Artirial (169/101 mmHg) fora da normalidade', '2022-06-03 22:41:06', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:41:06', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:41:29', 0),
(0, 'Paulo:Frequência cardiéca (106 bpm) fora da normalidade', '2022-06-03 22:41:29', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:41:29', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:41:52', 0),
(0, 'Paulo:Frequência cardiéca (154 bpm) fora da normalidade', '2022-06-03 22:41:52', 0),
(0, 'Paulo:Pressão Artirial (123/100 mmHg) fora da normalidade', '2022-06-03 22:41:52', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:41:52', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:42:15', 0),
(0, 'Paulo:Frequência cardiéca (177 bpm) fora da normalidade', '2022-06-03 22:42:15', 0),
(0, 'Paulo:Pressão Artirial (176/87 mmHg) fora da normalidade', '2022-06-03 22:42:15', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:42:15', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:42:39', 0),
(0, 'Paulo:Frequência cardiéca (183 bpm) fora da normalidade', '2022-06-03 22:42:39', 0),
(0, 'Paulo:Pressão Artirial (164/82 mmHg) fora da normalidade', '2022-06-03 22:42:39', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:42:39', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:43:02', 0),
(0, 'Paulo:Pressão Artirial (136/75 mmHg) fora da normalidade', '2022-06-03 22:43:02', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:43:02', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:43:25', 0),
(0, 'Paulo:Pressão Artirial (162/86 mmHg) fora da normalidade', '2022-06-03 22:43:25', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:43:25', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:43:48', 0),
(0, 'Paulo:Frequência cardiéca (109 bpm) fora da normalidade', '2022-06-03 22:43:48', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:43:48', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:44:11', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:44:11', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:44:34', 0),
(0, 'Paulo:Frequência cardiéca (58 bpm) fora da normalidade', '2022-06-03 22:44:34', 0),
(0, 'Paulo:Pressão Artirial (132/87 mmHg) fora da normalidade', '2022-06-03 22:44:34', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:44:34', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:44:57', 0),
(0, 'Paulo:Frequência cardiéca (198 bpm) fora da normalidade', '2022-06-03 22:44:57', 0),
(0, 'Paulo:Pressão Artirial (130/92 mmHg) fora da normalidade', '2022-06-03 22:44:57', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:44:57', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:45:21', 0),
(0, 'Paulo:Frequência cardiéca (104 bpm) fora da normalidade', '2022-06-03 22:45:21', 0),
(0, 'Paulo:Pressão Artirial (133/72 mmHg) fora da normalidade', '2022-06-03 22:45:21', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:45:21', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:45:44', 0),
(0, 'Paulo:Frequência cardiéca (150 bpm) fora da normalidade', '2022-06-03 22:45:44', 0),
(0, 'Paulo:Pressão Artirial (121/79 mmHg) fora da normalidade', '2022-06-03 22:45:44', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:45:44', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:46:07', 0),
(0, 'Paulo:Frequência cardiéca (192 bpm) fora da normalidade', '2022-06-03 22:46:07', 0),
(0, 'Paulo:Pressão Artirial (140/96 mmHg) fora da normalidade', '2022-06-03 22:46:07', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:46:07', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:46:30', 0),
(0, 'Paulo:Frequência cardiéca (173 bpm) fora da normalidade', '2022-06-03 22:46:30', 0),
(0, 'Paulo:Pressão Artirial (164/93 mmHg) fora da normalidade', '2022-06-03 22:46:30', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:46:30', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:46:53', 0),
(0, 'Paulo:Frequência cardiéca (136 bpm) fora da normalidade', '2022-06-03 22:46:53', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:46:53', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:47:16', 0),
(0, 'Paulo:Frequência cardiéca (176 bpm) fora da normalidade', '2022-06-03 22:47:16', 0),
(0, 'Paulo:Pressão Artirial (188/84 mmHg) fora da normalidade', '2022-06-03 22:47:16', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:47:16', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:47:39', 0),
(0, 'Paulo:Frequência cardiéca (171 bpm) fora da normalidade', '2022-06-03 22:47:39', 0),
(0, 'Paulo:Pressão Artirial (146/98 mmHg) fora da normalidade', '2022-06-03 22:47:39', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:47:39', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:48:02', 0),
(0, 'Paulo:Frequência cardiéca (112 bpm) fora da normalidade', '2022-06-03 22:48:02', 0),
(0, 'Paulo:Pressão Artirial (183/75 mmHg) fora da normalidade', '2022-06-03 22:48:02', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:48:02', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:48:25', 0),
(0, 'Paulo:Frequência cardiéca (169 bpm) fora da normalidade', '2022-06-03 22:48:25', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:48:25', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:48:49', 0),
(0, 'Paulo:Frequência cardiéca (119 bpm) fora da normalidade', '2022-06-03 22:48:49', 0),
(0, 'Paulo:Pressão Artirial (146/92 mmHg) fora da normalidade', '2022-06-03 22:48:49', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:48:49', 0),
(0, 'Paulo:Temperatura (27.13 ºC) fora da normalidade', '2022-06-03 22:49:12', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 22:49:12', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:49:35', 0),
(0, 'Paulo:Pressão Artirial (127/77 mmHg) fora da normalidade', '2022-06-03 22:49:35', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:49:35', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:49:58', 0),
(0, 'Paulo:Frequência cardiéca (149 bpm) fora da normalidade', '2022-06-03 22:49:58', 0),
(0, 'Paulo:Pressão Artirial (134/73 mmHg) fora da normalidade', '2022-06-03 22:49:58', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:49:58', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:50:21', 0),
(0, 'Paulo:Frequência cardiéca (176 bpm) fora da normalidade', '2022-06-03 22:50:21', 0),
(0, 'Paulo:Pressão Artirial (137/92 mmHg) fora da normalidade', '2022-06-03 22:50:21', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:50:21', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:50:44', 0),
(0, 'Paulo:Frequência cardiéca (107 bpm) fora da normalidade', '2022-06-03 22:50:44', 0),
(0, 'Paulo:Pressão Artirial (135/84 mmHg) fora da normalidade', '2022-06-03 22:50:44', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:50:44', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:51:07', 0),
(0, 'Paulo:Pressão Artirial (173/76 mmHg) fora da normalidade', '2022-06-03 22:51:07', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:51:07', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:51:30', 0),
(0, 'Paulo:Frequência cardiéca (107 bpm) fora da normalidade', '2022-06-03 22:51:30', 0),
(0, 'Paulo:Pressão Artirial (121/102 mmHg) fora da normalidade', '2022-06-03 22:51:30', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:51:30', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:51:53', 0),
(0, 'Paulo:Frequência cardiéca (120 bpm) fora da normalidade', '2022-06-03 22:51:53', 0),
(0, 'Paulo:Pressão Artirial (177/106 mmHg) fora da normalidade', '2022-06-03 22:51:53', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:51:53', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:52:17', 0),
(0, 'Paulo:Frequência cardiéca (136 bpm) fora da normalidade', '2022-06-03 22:52:17', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:52:17', 0),
(0, 'Paulo:Temperatura (27.06 ºC) fora da normalidade', '2022-06-03 22:52:40', 0),
(0, 'Paulo:Pressão Artirial (131/103 mmHg) fora da normalidade', '2022-06-03 22:52:40', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 22:52:40', 0),
(0, 'Paulo:Temperatura (27.00 ºC) fora da normalidade', '2022-06-03 22:53:03', 0),
(0, 'Paulo:Frequência cardiéca (127 bpm) fora da normalidade', '2022-06-03 22:53:03', 0),
(0, 'Paulo:Pressão Artirial (146/101 mmHg) fora da normalidade', '2022-06-03 22:53:03', 0),
(0, 'Paulo: Frequência Respiratória (27.00 ipm) fora da normalidade', '2022-06-03 22:53:03', 0),
(0, 'Paulo:Temperatura (26.94 ºC) fora da normalidade', '2022-06-03 22:53:26', 0),
(0, 'Paulo:Pressão Artirial (121/76 mmHg) fora da normalidade', '2022-06-03 22:53:26', 0),
(0, 'Paulo: Frequência Respiratória (26.94 ipm) fora da normalidade', '2022-06-03 22:53:26', 0),
(0, 'Paulo:Temperatura (26.94 ºC) fora da normalidade', '2022-06-03 22:53:49', 0),
(0, 'Paulo:Pressão Artirial (186/104 mmHg) fora da normalidade', '2022-06-03 22:53:49', 0),
(0, 'Paulo: Frequência Respiratória (26.94 ipm) fora da normalidade', '2022-06-03 22:53:49', 0),
(0, 'Paulo:Temperatura (26.87 ºC) fora da normalidade', '2022-06-03 22:54:12', 0),
(0, 'Paulo:Frequência cardiéca (175 bpm) fora da normalidade', '2022-06-03 22:54:12', 0),
(0, 'Paulo:Pressão Artirial (135/75 mmHg) fora da normalidade', '2022-06-03 22:54:12', 0),
(0, 'Paulo: Frequência Respiratória (26.87 ipm) fora da normalidade', '2022-06-03 22:54:12', 0),
(0, 'Paulo:Temperatura (26.87 ºC) fora da normalidade', '2022-06-03 22:54:35', 0),
(0, 'Paulo:Frequência cardiéca (129 bpm) fora da normalidade', '2022-06-03 22:54:35', 0),
(0, 'Paulo:Pressão Artirial (173/86 mmHg) fora da normalidade', '2022-06-03 22:54:35', 0),
(0, 'Paulo: Frequência Respiratória (26.87 ipm) fora da normalidade', '2022-06-03 22:54:35', 0),
(0, 'Paulo:Temperatura (26.87 ºC) fora da normalidade', '2022-06-03 22:54:58', 0),
(0, 'Paulo:Frequência cardiéca (162 bpm) fora da normalidade', '2022-06-03 22:54:58', 0),
(0, 'Paulo: Frequência Respiratória (26.87 ipm) fora da normalidade', '2022-06-03 22:54:58', 0),
(0, 'Paulo:Temperatura (26.87 ºC) fora da normalidade', '2022-06-03 22:55:21', 0),
(0, 'Paulo:Frequência cardiéca (138 bpm) fora da normalidade', '2022-06-03 22:55:21', 0),
(0, 'Paulo:Pressão Artirial (149/94 mmHg) fora da normalidade', '2022-06-03 22:55:21', 0),
(0, 'Paulo: Frequência Respiratória (26.87 ipm) fora da normalidade', '2022-06-03 22:55:21', 0),
(0, 'Paulo:Temperatura (26.81 ºC) fora da normalidade', '2022-06-03 22:55:44', 0),
(0, 'Paulo:Frequência cardiéca (105 bpm) fora da normalidade', '2022-06-03 22:55:44', 0),
(0, 'Paulo: Frequência Respiratória (26.81 ipm) fora da normalidade', '2022-06-03 22:55:44', 0),
(0, 'Paulo:Temperatura (26.81 ºC) fora da normalidade', '2022-06-03 22:56:08', 0),
(0, 'Paulo:Frequência cardiéca (55 bpm) fora da normalidade', '2022-06-03 22:56:08', 0),
(0, 'Paulo: Frequência Respiratória (26.81 ipm) fora da normalidade', '2022-06-03 22:56:08', 0),
(0, 'Paulo:Temperatura (26.81 ºC) fora da normalidade', '2022-06-03 22:56:31', 0),
(0, 'Paulo:Frequência cardiéca (134 bpm) fora da normalidade', '2022-06-03 22:56:31', 0),
(0, 'Paulo:Pressão Artirial (170/103 mmHg) fora da normalidade', '2022-06-03 22:56:31', 0),
(0, 'Paulo: Frequência Respiratória (26.81 ipm) fora da normalidade', '2022-06-03 22:56:31', 0),
(0, 'Paulo:Temperatura (26.81 ºC) fora da normalidade', '2022-06-03 22:56:54', 0),
(0, 'Paulo:Frequência cardiéca (160 bpm) fora da normalidade', '2022-06-03 22:56:54', 0),
(0, 'Paulo:Pressão Artirial (173/109 mmHg) fora da normalidade', '2022-06-03 22:56:54', 0),
(0, 'Paulo: Frequência Respiratória (26.81 ipm) fora da normalidade', '2022-06-03 22:56:54', 0),
(0, 'Paulo:Temperatura (26.81 ºC) fora da normalidade', '2022-06-03 22:57:18', 0),
(0, 'Paulo:Frequência cardiéca (140 bpm) fora da normalidade', '2022-06-03 22:57:18', 0),
(0, 'Paulo: Frequência Respiratória (26.81 ipm) fora da normalidade', '2022-06-03 22:57:18', 0),
(0, 'Paulo:Temperatura (26.81 ºC) fora da normalidade', '2022-06-03 22:57:41', 0),
(0, 'Paulo:Frequência cardiéca (144 bpm) fora da normalidade', '2022-06-03 22:57:41', 0),
(0, 'Paulo:Pressão Artirial (136/72 mmHg) fora da normalidade', '2022-06-03 22:57:41', 0),
(0, 'Paulo: Frequência Respiratória (26.81 ipm) fora da normalidade', '2022-06-03 22:57:41', 0),
(0, 'Paulo:Temperatura (26.81 ºC) fora da normalidade', '2022-06-03 22:58:04', 0),
(0, 'Paulo:Frequência cardiéca (107 bpm) fora da normalidade', '2022-06-03 22:58:04', 0),
(0, 'Paulo:Pressão Artirial (172/91 mmHg) fora da normalidade', '2022-06-03 22:58:04', 0),
(0, 'Paulo: Frequência Respiratória (26.81 ipm) fora da normalidade', '2022-06-03 22:58:04', 0),
(0, 'Paulo:Temperatura (26.81 ºC) fora da normalidade', '2022-06-03 22:58:28', 0),
(0, 'Paulo:Frequência cardiéca (141 bpm) fora da normalidade', '2022-06-03 22:58:28', 0),
(0, 'Paulo: Frequência Respiratória (26.81 ipm) fora da normalidade', '2022-06-03 22:58:28', 0),
(0, 'Paulo:Temperatura (26.81 ºC) fora da normalidade', '2022-06-03 22:58:51', 0),
(0, 'Paulo:Frequência cardiéca (151 bpm) fora da normalidade', '2022-06-03 22:58:51', 0),
(0, 'Paulo: Frequência Respiratória (26.81 ipm) fora da normalidade', '2022-06-03 22:58:51', 0),
(0, 'Paulo:Temperatura (26.81 ºC) fora da normalidade', '2022-06-03 22:59:37', 0),
(0, 'Paulo:Frequência cardiéca (149 bpm) fora da normalidade', '2022-06-03 22:59:37', 0),
(0, 'Paulo: Frequência Respiratória (26.81 ipm) fora da normalidade', '2022-06-03 22:59:37', 0),
(0, 'Paulo:Temperatura (26.81 ºC) fora da normalidade', '2022-06-03 23:00:00', 0),
(0, 'Paulo:Frequência cardiéca (118 bpm) fora da normalidade', '2022-06-03 23:00:00', 0),
(0, 'Paulo: Frequência Respiratória (26.81 ipm) fora da normalidade', '2022-06-03 23:00:00', 0),
(0, 'Paulo:Temperatura (26.81 ºC) fora da normalidade', '2022-06-03 23:00:23', 0),
(0, 'Paulo:Frequência cardiéca (52 bpm) fora da normalidade', '2022-06-03 23:00:23', 0),
(0, 'Paulo: Frequência Respiratória (26.81 ipm) fora da normalidade', '2022-06-03 23:00:23', 0),
(0, 'Paulo:Temperatura (26.81 ºC) fora da normalidade', '2022-06-03 23:00:46', 0),
(0, 'Paulo:Frequência cardiéca (103 bpm) fora da normalidade', '2022-06-03 23:00:46', 0),
(0, 'Paulo: Frequência Respiratória (26.81 ipm) fora da normalidade', '2022-06-03 23:00:46', 0),
(0, 'Paulo:Temperatura (27.50 ºC) fora da normalidade', '2022-06-03 23:01:35', 0),
(0, 'Paulo:Pressão Artirial (126/103 mmHg) fora da normalidade', '2022-06-03 23:01:35', 0),
(0, 'Paulo: Frequência Respiratória (27.50 ipm) fora da normalidade', '2022-06-03 23:01:35', 0),
(0, 'Paulo:Temperatura (27.37 ºC) fora da normalidade', '2022-06-03 23:01:58', 0),
(0, 'Paulo:Frequência cardiéca (50 bpm) fora da normalidade', '2022-06-03 23:01:59', 0),
(0, 'Paulo:Pressão Artirial (157/82 mmHg) fora da normalidade', '2022-06-03 23:01:59', 0),
(0, 'Paulo: Frequência Respiratória (27.37 ipm) fora da normalidade', '2022-06-03 23:01:59', 0),
(0, 'Paulo:Frequência cardiéca (139 bpm) fora da normalidade', '2022-06-03 23:02:45', 0),
(0, 'Paulo:Pressão Artirial (144/76 mmHg) fora da normalidade', '2022-06-03 23:02:45', 0),
(0, 'Paulo: Frequência Respiratória (27.19 ipm) fora da normalidade', '2022-06-03 23:02:45', 0),
(0, 'Paulo:Frequência cardiéca (185 bpm) fora da normalidade', '2022-06-03 23:03:08', 0),
(0, 'Paulo:Pressão Artirial (163/88 mmHg) fora da normalidade', '2022-06-03 23:03:08', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 23:03:08', 0),
(0, 'Paulo:Pressão Artirial (169/103 mmHg) fora da normalidade', '2022-06-03 23:03:32', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 23:03:32', 0),
(0, 'Paulo:Frequência cardiéca (188 bpm) fora da normalidade', '2022-06-03 23:03:55', 0);
INSERT INTO `alerta` (`id`, `menssagem`, `data`, `view`) VALUES
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 23:03:55', 0),
(0, 'Paulo:Frequência cardiéca (199 bpm) fora da normalidade', '2022-06-03 23:04:18', 0),
(0, 'Paulo:Pressão Artirial (125/84 mmHg) fora da normalidade', '2022-06-03 23:04:18', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 23:04:18', 0),
(0, 'Paulo: Frequência Respiratória (27.06 ipm) fora da normalidade', '2022-06-03 23:04:41', 0),
(0, 'Paulo:Frequência cardiéca (123 bpm) fora da normalidade', '2022-06-03 23:05:04', 0),
(0, 'Paulo:Pressão Artirial (187/90 mmHg) fora da normalidade', '2022-06-03 23:05:04', 0),
(0, 'Paulo: Frequência Respiratória (27.13 ipm) fora da normalidade', '2022-06-03 23:05:04', 0);

-- --------------------------------------------------------

--
-- Table structure for table `anotacao_paciente`
--

CREATE TABLE `anotacao_paciente` (
  `id` int(11) NOT NULL,
  `p_nome` varchar(191) NOT NULL,
  `idade` int(11) NOT NULL,
  `sexo` varchar(191) NOT NULL,
  `responsavel` varchar(191) NOT NULL,
  `descricao` text DEFAULT NULL,
  `historico` text DEFAULT NULL,
  `navbar_status` tinyint(1) DEFAULT 0,
  `status` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `anotacao_paciente`
--

INSERT INTO `anotacao_paciente` (`id`, `p_nome`, `idade`, `sexo`, `responsavel`, `descricao`, `historico`, `navbar_status`, `status`, `created_at`) VALUES
(1, 'pedro', 19, '1', 'Dr Mario lima', 'sofre de doenças cardiovasculares', '-Seu pai e alguns familiares sofrem da mesma doença.', 0, 0, '2022-06-03 17:40:47'),
(2, 'cenira', 29, '0', 'Dr Austolino', 'Problema com constantes bruscas oscilações de temperatura corporal', '-Problemas hirectariedado na familia', 0, 0, '2022-06-03 19:51:37');

-- --------------------------------------------------------

--
-- Table structure for table `niveis_acessos`
--

CREATE TABLE `niveis_acessos` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `role_as` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `niveis_acessos`
--

INSERT INTO `niveis_acessos` (`id`, `nome`, `created_at`, `role_as`) VALUES
(1, 'admin', '2021-10-26 21:02:03', NULL),
(2, 'cliente', '2021-10-26 21:02:03', NULL),
(3, 'colaborador', '2021-10-26 21:02:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sensordata`
--

CREATE TABLE `sensordata` (
  `id` int(6) UNSIGNED NOT NULL,
  `sensor` varchar(30) NOT NULL,
  `location` varchar(30) NOT NULL,
  `value1` varchar(10) DEFAULT NULL,
  `value2` varchar(10) DEFAULT NULL,
  `value3` varchar(10) DEFAULT NULL,
  `value4` varchar(191) NOT NULL,
  `reading_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sensordata`
--

INSERT INTO `sensordata` (`id`, `sensor`, `location`, `value1`, `value2`, `value3`, `value4`, `reading_time`) VALUES
(1, 'Paulo', 'Sala 1', '27.31', '192', '156/85', '9', '2022-06-03 23:16:47'),
(2, 'Paulo', 'Sala 1', '27.31', '108', '130/103', '9', '2022-06-03 23:17:10'),
(3, 'Paulo', 'Sala 1', '27.31', '184', '173/73', '10', '2022-06-03 23:17:33'),
(4, 'Paulo', 'Sala 1', '27.31', '182', '115/107', '7', '2022-06-03 23:18:25'),
(26, 'Paulo', 'Sala 1', '27.25', '57', '107/84', '6', '2022-06-03 23:26:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `p_nome` varchar(240) NOT NULL,
  `Apedilio` varchar(240) NOT NULL,
  `email` varchar(240) NOT NULL,
  `senha` varchar(240) NOT NULL,
  `role_as` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `p_nome`, `Apedilio`, `email`, `senha`, `role_as`, `status`, `created_at`) VALUES
(1, 'Emanuel', 'Miranda', 'emanuel@gmail.com', '12345', 1, 0, '0000-00-00 00:00:00'),
(8, 'Natalino', 'Miranda', 'natalino@gmail.com', '12345', 0, 0, '2022-06-02 15:06:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anotacao_paciente`
--
ALTER TABLE `anotacao_paciente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `niveis_acessos`
--
ALTER TABLE `niveis_acessos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sensordata`
--
ALTER TABLE `sensordata`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anotacao_paciente`
--
ALTER TABLE `anotacao_paciente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `niveis_acessos`
--
ALTER TABLE `niveis_acessos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sensordata`
--
ALTER TABLE `sensordata`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
