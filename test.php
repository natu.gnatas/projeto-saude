<?php
//include('autenticacao.php');
include('includes/header.php');
include('admin/configuracao/dbcon.php');
?>

<div class="container-fluid px-4">
    <h4 class="mt-4">Usuários</h4>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Dashboard</li>
        <li class="breadcrumb-item active">Dados do Paciente</li>
    </ol>
    <div class="row">

        <div class="col-md-12">

            <?php include('menssagem.php'); ?>

            <div class="card">
                <div class="card-header">
                    <h4>Dados do Paciêntes</h4>
                    <a href="registrar-add.php" class="btn btn-primary float-end">Adicionar Admin/Usuário</a>
                </div>
                <div class="card-body">

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Data $ Hora</th>
                                <th>Paciente</thh>
                                <th>Localização</th>
                                <th>Temperatura &deg;C</th>
                                <th>Batimento Cardiéco</th>
                                <th>Pressão Artirial</th>
                                <th>Frequência Cardiéca</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            
                            $query = "SELECT id, sensor, location, value1, value2, value3, value4, reading_time FROM sensordata ORDER BY id DESC";
                            //$query = "SELECT *FROM sensordata";
                            $query_run = mysqli_query($conn, $query);

                            if (mysqli_num_rows($query_run) > 0) {
                                foreach ($query_run as $row) {
                            ?>
                                    <tr>
                                        <td><?= $row['id']; ?></td>
                                        <td><?= $row["reading_time"]; ?></td>
                                        <td><?= $row["sensor"]; ?></td>
                                        <td><?= $row["location"]; ?></td>
                                        <td><?= $row["value1"]; ?></td>
                                        <td><?= $row["value2"]; ?></td>  
                                        <td><?= $row["value3"]; ?></td>
                                        <td><?= $row["value4"]; ?></td>
                                        
                                    </tr>
                                <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="6">Nenhuma gravação encontrada</td>
                                </tr>
                            <?php
                            }
                            ?>
                            <tr>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

<?php
//include('includes/fooder.php');
//include('includes/script.php');
?>