<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"> <strong> Patient Monitoring</strong></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="index.php"> <strong>Home</strong> </a>
        </li>
        <li class="nav-item">
          <!--<a class="nav-link" href="#">Link</a>-->
        </li>
        <li class="nav-item dropdown">
         
        <?php if (isset($_SESSION['auth_user'])) : ?>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            <strong> <?= $_SESSION['auth_user']['user_name'];  ?></strong>
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="#"> <strong>Meu Perfil</strong> </a></li>
              <li>
                <form action="todocodigo.php" method="POST">
                  <button type="submit" name="sair_btn" class="dropdown-item">Sair</button>
                </form>
              </li>
            </ul>
          </li>
        <?php else : ?>
        
          <li class="nav-item">
            <a class="nav-link" href="login2.php"> Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="registrar-add.php">Registrar</a>
          </li>
        
        <?php endif; ?>

            
           <!-- <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#!">Configurações</a></li>
             <li><a class="dropdown-item" href="#!"></a></li>
                <li>
                    <hr class="dropdown-divider" />
                </li>
                <li><a class="dropdown-item" href="../todocodigo.php">Sair</a></li>
            </ul>-->

        </li>
      </ul>
      
    </div>
  </div>
</nav>